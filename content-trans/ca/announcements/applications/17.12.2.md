---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE distribueix les aplicacions 17.12.2 del KDE
layout: application
title: KDE distribueix les aplicacions 17.12.2 del KDE
version: 17.12.2
---
8 de febrer de 2018. Avui KDE distribueix la segona actualització d'estabilització per a les <a href='../17.12.0'>aplicacions 17.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha unes 20 esmenes registrades d'errors que inclouen millores al Kontact, Dolphin, Gwenview, KGet i Okular entre d'altres.
