---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: Es distribueixen les aplicacions 18.12.2 del KDE.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDE distribueix les aplicacions 18.12.2 del KDE
version: 18.12.2
---
{{% i18n_date %}}

Avui, KDE distribueix la segona actualització d'estabilització per a les <a href='../18.12.0'>Aplicacions 18.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, i proporciona una actualització segura i millor per a tothom.

Hi ha més d'una dotzena d'esmenes registrades d'errors que inclouen millores al Kontact, Ark, Konsole, Lokalize i Umbrello, entre d'altres.

Les millores inclouen:

- L'Ark ja no suprimeix els fitxers desats des de dins del visualitzador incrustat</li>
- La llibreta d'adreces ara recorda els aniversaris en fusionar contactes</li>
- S'han esmenat diverses actualitzacions de visualització de diagrames que mancaven a l'Umbrello</li>
