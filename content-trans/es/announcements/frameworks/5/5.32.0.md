---
aliases:
- ../../kde-frameworks-5.32.0
date: 2017-03-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Se han implementado etiquetas anidadas

### Iconos Brisa

- Se han añadido iconos para la caja fuerte de Plasma
- Se ha cambiado el nombre de los iconos para carpetas cifradas y descifradas
- Se ha añadido el icono de 22px para torrentes
- Añadir iconos para nm-tray (fallo 374672).
- Gestión de color: se han eliminado enlaces no definidos (fallo 374843).
- System-run es ahora una acción hasta &lt;=32px y 48px un icono de aplicación (fallo 375970).

### Módulos CMake adicionales

- Detectar «inotify».
- Revertir «Marcar automáticamente clases con funciones virtuales puras como abstractas».

### KActivitiesStats

- Permitir planificar con anticipación y establecer el orden de los artículos que aún no están en la lista.

### KArchive

- Corregir una fuga de memoria potencial a la que apuntaba «limitedDev».

### KCMUtils

- Se ha corregido un cuelgue potencial en los KCM en QML cuando cambia la paleta de la aplicación.

### KConfig

- KConfig: detener la exportación y la instalación de KConfigBackend.

### KConfigWidgets

- KColorScheme: usar por omisión el esquema de la aplicación si se ha definido con «KColorSchemeManager» (error 373764).
- KConfigDialogManager: Obtener la señal de cambio de «metaObject» o de una propiedad especial.
- Se ha corregido la comprobación de errores «KCModule::setAuthAction».

### KCoreAddons

- Excluir (6) del reconocimiento de emoticonos.
- KDirWatch: corregir fuga de memoria al destruir.

### Soporte de KDELibs 4

- Se ha corregido un error en «kfiledialog.cpp» que causaba un fallo de la aplicación si se usaban widgets nativos.

### KDocTools

- meinproc5: enlace a los archivos, no a la biblioteca (fallo 377406).
- Se ha eliminado la biblioteca estática «KF5::XsltKde».
- Exportar una biblioteca compartida adecuada para «KDocTools».
- Adaptar a registro por categorías y limpiar archivos de cabecera.
- Se ha añadido una función para extraer un archivo único.
- Hacer que falle la compilación antes si no se encuentra «xmllint» (error 376246).

### KFileMetaData

- Nuevo encargado para «kfilemetadata».
- [ExtractorCollection] Usar la herencia de tipos MIME para devolver complementos.
- Se ha añadido una nueva propiedad «DiscNumber» para archivos de sonido de álbumes multidisco.

### KIO

- KCM de cookies: desactivar el botón «borrar» si no existe un elemento actual.
- kio_help: Usar la nueva biblioteca compartida exportada por «KDocTools».
- kpac: Sanear URL antes de pasarlas a «FindProxyForURL» (corrección de seguridad).
- Importar «ioslave» remoto de «plasma-workspace».
- kio_trash: Implementar cambio de nombre de archivos y directorios del nivel superior.
- PreviewJob: Eliminar el tamaño máximo para los archivos locales de forma predeterminada.
- DropJob: Permitir añadir acciones de aplicaciones en un menú abierto.
- ThumbCreator: Marcar como obsoleto «DrawFrame», tal como se discute en https://git.reviewboard.kde.org/r/129921/

### KNotification

- Permitir el uso de portales «flatpak»
- Enviar «desktopfilename» como parte de las sugerencias «notifyByPopup».
- [KStatusNotifierItem] Restaurar las ventanas minimizadas como normales.

### Framework KPackage

- Acabar con el soporte para abrir paquetes comprimidos.

### KTextEditor

- Recordar el tipo de archivo definido por el usuario a través de las sesiones.
- Reiniciar el tipo de archivo al abrir una URL.
- Se ha añadido un «getter» para el valor de configuración del contador de palabras
- Conversión consistente desde/hacia cursor hacia/desde coordenadas.
- Actualizar el tipo de archivo al guardar solo si cambia la ruta.
- Permitir el uso de archivos de configuración «EditorConfig» (para más detalles: http://editorconfig.org/).
- Añadir FindEditorConfig a ktexteditor.
- Corrección: La acción «emmetToggleComment» no funciona (error 375159).
- Usar capitalización al estilo de frases con los textos de etiquetas de los campos de edición.
- Invertir el significado de «:split» y «:vsplit» para que coincida con las acciones de «vi» y de Kate.
- Usar «log2()» de C++11 en lugar de «log() / log(2)».
- KateSaveConfigTab: Situar un espaciador detrás del último grupo en la pestaña «Avanzado», no dentro.
- KateSaveConfigTab: Eliminar el margen incorrecto alrededor del contenido de la pestaña «Avanzado».
- Subpágina de configuración de bordes: Corregir la lista desplegable de visibilidad de la barra de desplazamiento, que estaba mal situada.

### KWidgetsAddons

- KToolTipWidget: Ocultar la ayuda emergente en «enterEvent» si «hideDelay» es cero.
- Se ha corregido la pérdida del foco de «KEditListWidget» al pulsar los botones.
- Se ha añadido la descomposición de sílabas hangul en hangul jamo.
- KMessageWidget: Se ha corregido el comportamiento cuando se producen llamadas de solapamiento de «animatedShow/animatedHide».

### KXMLGUI

- No usar claves de «KConfig» con barras inversas.

### NetworkManagerQt

- Sincronización de introspecciones y de los archivos generados con NM 1.6.0. 
- Gestor: Corregir la doble emisión de «deviceAdded» cuando se reinicia NM.

### Framework de Plasma

- Definir las sugerencias predeterminadas cuando «repr» no exporta «Layout.*» (error 377153).
- Posibilidad de definir «expanded=false» en los contenedores.
- [Menú] Mejorar la corrección del espacio disponible para «openRelative».
- Mover la lógica de «setImagePath» a «updateFrameData()» (error 376754).
- IconItem: Se ha añadido la propiedad «roundToIconSize».
- [SliderStyle] Permitir que se suministre un elemento «hint-handle-size».
- Conectar todas las conexiones a acciones en «QMenuItem::setAction».
- [ConfigView] Respetar las restricciones del módulo de control de KIOSK.
- Se ha corregido la desactivación de la animación de la ruleta cuando el indicador de ocupado no tiene opacidad.
- [FrameSvgItemMargins] No actualizar cuando se recibe «repaintNeeded».
- Se han añadido iconos de miniaplicación para la caja fuerte de Plasma.
- Migrar «AppearAnimation» y «DisappearAnimation» a «Animators».
- Alinear el borde inferior con el borde superior de «visualParent» en el caso de «TopPosedLeftAlignedPopup».
- [ConfigModel] Emitir «dataChanged» cuando cambia una «ConfigCategory».
- [ScrollViewStyle] Evaluar la propiedad «frameVisible»
- [Estilos de botones] Usar «Layout.fillHeight» en lugar de «parent.height» en las disposiciones (fallo 375911).
- [ContainmentInterface] Alinear también el menú de contexto del contenedor al panel.

### Prison

- Se ha corregido la versión mínima de Qt.

### Solid

- Los disquetes se muestran ahora como «Disquete» en lugar de «Medio extraíble de 0 B»).

### Resaltado de sintaxis

- Añadir más palabras clave. Desactivar la corrección ortográfica para las palabras clave.
- Añadir más palabras clave.
- Se ha añadido la extensión de archivo *.RHTML para el resaltado de Rails (fallo 375266).
- Actualizar el resaltado de sintaxis para SCSS y CSS (fallo 376005)
- Resaltado sintáctico de «less»: Corregir que los comentarios de una sola línea empiecen nuevas regiones.
- Resaltado de LaTeX: se ha corregido el entorno de «alignat» (error 373286)

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
