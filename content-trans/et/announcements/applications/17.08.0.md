---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE toob välja KDE rakendused 17.08.2
layout: application
title: KDE toob välja KDE rakendused 17.08.2
version: 17.08.0
---
17. august 2017. KDE rakendused 17.08 on kohal. Üldiselt püüdsime muuta nii rakendused kui ka teegid stabiilsemaks ja kasutuskindlamaks. Ebakõlasid eemaldades ja kõigi kasutajate tagasisidet kuulates oleme loodetavasti suutnud teha KDE rakendused viimitletumaks ja mugavamaks. Nautige oma uusi rakendusi!

### Rohkem postimist KDE Frameworks 5 peale

Meil on hea meel teada anda, et järgmised rakendused, mis seni töötasid kdelibs4 peal, tuginevad nüüd KDE Frameworks 5-le: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat ja umbrello. Täname kõiki ränka vaeva näinud arendajaid, kes ohverdasid oma aega ja teadmisi, et see kõik teoks saaks.

### What's new in KDE Applications 17.08

#### Dolphin

Dolphini arendajad annavad teada, et Dolphin näitab nüüd prügikastis "kustutamise aega" ning samuti näidatakse "loomise aega", kui operatsioonisüsteem seda toetab (nagu näiteks BSD perekond).

#### KIO-Extras

Kio-Extras toetab nüüd palju paremini Samba jagatud ressursse.

#### KAlgebra

KAlgebra arendajad on täiustanud töölaua Kirigami-põhist kasutajaliidest ning pakuvad nüüd koodi lõpetamist.

#### Kontact

- KMailTransport'is on arendajad taastanud Akonadi transpordiviiside toetuse, hakanud toetama pluginaid ja toonud tagasi sendmail'i kirjade saatmise toetuse.
- Sieve redaktori puhul on ära parandatudrohkelt vigu skriptide automaatse loomise võimaluse juures, Lisaks vigade parandamisele lisati regulaaravaldiste redaktoris rea kaupa redigeerimise toetus.
- KMailis taastati välise redaktori kasutamise võimalus plugina abil.
- Akonadi impordinõustaja pakub nüüd pluginana "kõigi teisendajate teisendamist", mis lubab arendajatele palju hõlpsamalt uusi teisendajaid luua.
- Rakendused sõltuvad nüüd Qt 5.7-st. Arendajad on parandanud hulga kompileerimistõrkeid Windowsis. Kogu kdepimi kompileerimine Windowsis ei ole veel võimalik, aga arendajad on saavutanud suurt edu. Alustuseks on loodud selleks otstarbeks Crafti retsept. Rohkelt aitas vigu parandada koodi kaasajastamine (C++11). Waylandi toetus tuleb koos Qt 5.9-ga. Kdepim-runtime lisas Facebooki ressursi.

#### Kdenlive

Kdenlive'i meeskond parandas tõrkuva "peatamisefekti". Viimastes versioonides ei olnud enam võimalik muuta peatamisefekti kaadrit. Nüüd on kiirklahviga lubatud kaadri eraldamine. Samuti saab kasutaja kiirklahviga salvestada oma ajatelje ekraanipilte, mille korral nimi pakutakse välja kaadri numbri järgi (<a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. Allalaaditud ülemineku-luma parandus ei ole liideses otseselt näha (<a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Ka heli klõpsimise viga parandati ära (see nõuab praegu MLT sõltuvuse paigaldamist gitist, kuni MLT uue versiooni välja laseb) (<a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>).

#### Krfb

Arendajad lõpetasid X11 plugina portimise Qt5 peale ning krfb töötab taas X11 taustaprogrammiga, mis on palju kiirem kui Qt plugin. Rakendus pakub uut seadistustelehekülge, mis lubab kasutajal muuta kaadripuhvri plugina eelistust. 

#### Konsool

Konsool võimaldab nüüd ajaloos piiramatut tagasikerimist ka üle 2 GB (32-bitise) piirangu. Konsool lubab nüüd kasutajal salvestada ajaloofaile suvalisse asukohta. Samuti parandati üks tagasilangus ning Konsool lubab jälle KonsolePartil välja kutsuda profiili haldamise dialoogi.

#### KAppTemplate

In KAppTemplate there is now an option to install new templates from the filesystem. More templates have been removed from KAppTemplate and instead integrated into related products; ktexteditor plugin template and kpartsapp template (ported to Qt5/KF5 now) have become part of KDE Frameworks KTextEditor and KParts since 5.37.0. These changes should simplify creation of templates in KDE applications.

### Vigadest vabaks

Üle 80 vea parandati sellistes rakendustes, nagu Kontacti komplekt, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsool jt!

### Täielik muudatuste logi
