---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE toob välja KDE rakendused 18.04.2
layout: application
title: KDE toob välja KDE rakendused 18.04.2
version: 18.04.2
---
June 7, 2018. Today KDE released the second stability update for <a href='../18.04.0'>KDE Applications 18.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Umbes 25 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Cantori, Dolphini, Gwenview, KGpg, Kigi, Konsooli, Lokalize ja Okulari täiustused.

Täiustused sisaldavad muu hulgas:

- Gwenview pilditoiminguid saab nüüd pärast tagasivõtmist uuesti teha
- KGpg suudab nüüd tõrgeteta lahti krüptida versioonipäiseta kirju
- Maxima maatriksitega Cantori töölehtede eksport LaTeXi vormingusse parandati ära
