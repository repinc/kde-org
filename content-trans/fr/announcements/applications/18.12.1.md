---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE publie la version 18.12.2 des applications de KDE.
layout: application
major_version: '18.12'
title: KDE publie la version 18.12.1 des applications de KDE.
version: 18.12.1
---
{{% i18n_date %}}

Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../18.12.0'>applications de KDE %[2 ]s</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traductions, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à Kontact, Cantor, Dolphin, JuK, Kdenlive, Konsole, Okular et bien d'autres.

Les améliorations incluses sont :

- Akregator fonctionne maintenant avec « WebEngine » à partir de Qt 5.11 et versions ultérieures.
- Le tri de colonnes dans le lecteur de musique JuK a été corrigé.
- Konsole affiche de nouveau correctement les caractères en mode bloc.
