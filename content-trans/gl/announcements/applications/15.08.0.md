---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE Ships Applications 15.08.0.
layout: application
release: applications-15.08.0
title: KDE publica a versión 15.08.0 das aplicacións de KDE
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphin coa súa nova aparencia, agora baseado na versión 5 das infraestruturas de KDE` class="text-center" width="600px" caption=`Dolphin coa súa nova aparencia, agora baseado na versión 5 das infraestruturas de KDE`>}}

19 de agosto de 2015. Hoxe KDE publicou a versión 15.08 das aplicacións de KDE.

With this release a total of 107 applications have been ported to <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. The team is striving to bring the best quality to your desktop and these applications. So we're counting on you to send your feedback.

With this release there are several new additions to the KDE Frameworks 5-based applications list, including <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>the Kontact Suite</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Vista previa técnica da colección Kontact

Over the past several months the KDE PIM team did put a lot of effort into porting Kontact to Qt 5 and KDE Frameworks 5. Additionally the data access performance got improved considerably by an optimized communication layer. The KDE PIM team is working hard on further polishing the Kontact Suite and is looking forward to your feedback. For more and detailed information about what changed in KDE PIM see <a href='http://www.aegiap.eu/kdeblog/'>Laurent Montels blog</a>.

### Kdenlive e Okular

This release of Kdenlive includes lots of fixes in the DVD wizard, along with a large number of bug-fixes and other features which includes the integration of some bigger refactorings. More information about Kdenlive's changes can be seen in its <a href='https://kdenlive.org/discover/15.08.0'>extensive changelog</a>. And Okular now supports Fade transition in the presentation mode.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku cun quebracabezas de carácter` class="text-center" width="600px" caption=`KSudoku cun quebracabezas de carácter`>}}

### Dolphin, educación e xogos

Dolphin was as well ported to KDE Frameworks 5. Marble got improved <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a> support as well as better support for annotations, editing and KML overlays.

Ark goza dun impresionante número de remisións entre as que hai moitas correccións pequenas. KStars recibiu un gran número de revisións, que inclúen a mellora do algoritmo plano de ADU e a comprobación de valores que superan os límites, gardando os límites de volta do meridiano, guía de desviación e HFR de enfoque automático no ficheiro de secuencia e engadir funcionalidade de taxa de telescopio e desestacionamento. KSudoku mellorou. Entre as remisións están engadir unha interface gráfica de usuario e un motor para completar quebracabezas matesdocu e sudoku imposíbel, e engadiuse un novo resolvedor baseado no algoritmo de ligazóns bailarinas (DLX) de Donald Knuth.

### Outras versións

Xunto con esta versión publicarase a última versión LTS de Plasma 4, a 4.11.22.
