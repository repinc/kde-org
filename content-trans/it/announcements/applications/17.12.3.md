---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE rilascia KDE Applications 17.12.3
layout: application
title: KDE rilascia KDE Applications 17.12.3
version: 17.12.3
---
8 marzo 2018. Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../17.12.0'>KDE Applications 17.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Circa venticinque errori corretti includono, tra gli altri, miglioramenti a Kontact, Dolphin, Gwenview, JuK, KGet, Okular e Umbrello.

I miglioramenti includono:

- Akregator non cancella più l'elenco di fonti feeds.opml dopo un errore
- La modalità a schermo intero di Gwenview ora opera sul nome file corretto dopo la rinomina
- Sono stati identificati e risolti diversi errori in Okular
