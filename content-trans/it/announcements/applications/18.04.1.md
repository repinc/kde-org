---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE rilascia KDE Applications 18.04.1
layout: application
title: KDE rilascia KDE Applications 18.04.1
version: 18.04.1
---
10 maggio 2018. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../18.04.0'>KDE Applications 18.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Circa venti errori corretti includono, tra gli altri, miglioramenti a Kontact, Cantor, Dolphin, Gwenview, JuK, Okular e Umbrello.

I miglioramenti includono:

- Voci duplicate nel pannello dei luoghi di Dolphin non portano più a blocchi del programma
- È stato corretto un vecchio problema legato al ricaricamento di file SVG in Gwenview
- L'importatore C++ di Umbrello ora riconosce la parola chiave «explicit»
