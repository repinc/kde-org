---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE Ships Applications 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၈.၁၂.၃ တင်ပို့ခဲ့သည်
version: 18.12.3
---
{{% i18n_date %}}

Today KDE released the third stability update for <a href='../18.12.0'>KDE Applications 18.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than twenty recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Filelight, JuK, Lokalize, Umbrello, among others.

တိုးတက်မှုများ -

- Loading of .tar.zstd archives in Ark has been fixed
- Dolphin no longer crashes when stopping a Plasma activity
- Switching to a different partition can no longer crash Filelight
