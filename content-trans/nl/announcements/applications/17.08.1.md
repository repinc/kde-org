---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE stelt KDE Applicaties 17.08.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.08.1 beschikbaar
version: 17.08.1
---
7 september 2017. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../17.08.0'>KDE Applicaties 17.08</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 20 aangegeven reparaties van bugs, inclusief verbeteringen aan Kontact, Konsole, KWalletManager, Okular, Umbrello, KDE games, naast anderen.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.36.
