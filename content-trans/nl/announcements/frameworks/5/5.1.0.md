---
aliases:
- ../../kde-frameworks-5.1
- ./5.1
customIntro: true
date: '2014-08-08'
description: KDE stelt de tweede uitgave van Framework 5 beschikbaar
layout: framework
qtversion: 5.2
title: Tweede uitgave van KDE Framework 5
---
7 augustus 2014. KDE kondigt vandaag de tweede uitgave van KDE Frameworks 5 aan. In overeenstemming met het geplande uitgavebeleid voor KDE Frameworks komt deze uitgave een maand na de initiële versie en heeft zowel reparaties van bugs als nieuwe functies.

{{% i18n "annc-frameworks-intro" "60" "/announcements/frameworks/5/5.0" %}}

## {{< i18n "annc-frameworks-new" >}}

This release, versioned 5.1, comes with a number of bugfixes and new features including:

- KTextEditor: Major refactorings and improvements of the vi-mode
- KAuth: Now based on PolkitQt5-1
- New migration agent for KWallet
- Windows compilation fixes
- Translation fixes
- New install dir for KXmlGui files and for AppStream metainfo
- <a href='http://www.proli.net/2014/08/04/taking-advantage-of-opengl-from-plasma/'>Plasma Taking advantage of OpenGL</a>
