---
aliases:
- ../../kde-frameworks-5.62.0
date: 2019-09-14
layout: framework
libCount: 70
---
### Attica

- Het attica pkgconfig-bestand repareren

### Baloo

- Repareert een crash in Peruse getriggerd door baloo

### Breeze pictogrammen

- Nieuwe activiteiten en virtuele bureaubladpictogrammen
- Laat kleine recente documentpictogrammane lijken op documenten en verbeter klokemblemen
- Een nieuw pictogram "Recente mappen" aanmaken (bug 411635)
- Pictogram voor "preferences-desktop-navigation" (bug 402910)
- 22px dialoog-scripts toevoegen, scriptacties/plaatspictogrammen wijzigen om er mee overeenkomen
- "user-trash" pictogram verbeteren
- Leeg/gevuld stijl voor monochrome lege/gevulde prullenbak
- Notificatiepictogrammen de stijl outline laten gebruiken
- Pictogrammen van prullenbak van de gebruiker laten lijken op prullenbak (bug 399613)
- Breeze pictogrammen voor ROOT-cern-bestanden
- applets/22/computer verwijderen (bug 410854)
- Pictogrammen view-barcode-qr toevoegen
- Krita is afgesplitst van Calligra en gebruikt nu de naam Krita in plaats van calligrakrita (bug 411163)
- Batterij-omhoog-pictogrammen toevoegen (bug 411051)
- Laat "monitor" een koppeling naar pictogram "computer"
- Pictogrammen voor FictionBook 2 toevoegen
- Pictogram voor kuiviewer, heeft ook bijwerken -&gt; bug 407527
- Symbolische koppeling "port" naar "anker", die meer toepasselijke pictogrammen laat zien
- Pictogram Radio naar apparaat wijzigen, meer groottes toevoegen
- Laat pictogram <code>pin</code> wijzen naar een pictogram die lijkt op een pin, niet iets niet gerelateerd
- Ontbrekend cijfer en pixel-perfecte uitlijning van diepte-actie-pictogrammen (bug 406502)
- 16px map-activiteiten laten meer laten lijken op grotere afmetingen
- latte-dock pictogram uit latte-dock-repo voor kde.org/applications toevoegen
- pictogram voor kdesrc-build gebruikt door kde.org/applications opnieuw tekenen
- Media-show-active-track-amarok naar media-track-show-active hernoemen

### Extra CMake-modules

- ECMAddQtDesignerPlugin: codesample indirect doorgeven via variabelenaam argument
- 'lib' als standaard LIBDIR behouden op Arch Linux gebaseerde systemen
- Autorcc standaard inschakelen
- Installatielocatie voor JAR/AAR bestanden voor Android definiëren
- ECMAddQtDesignerPlugin toevoegen

### KActivitiesStats

- Term::Type::files() en Term::Type::directories() toevoegen om alleen mappen te filteren of ze uit te sluiten
- @since 5.62 toevoegen voor nieuw toegevoegde setters
- Juiste logging toevoegen met ECMQtDeclareLoggingCategory
- Setter toevoegen aan afvraagvelden Type, Activiteit, Agent en UrlFilter
- Speciale waardeconstanten in terms.cpp
- Op datumreeks filteren van hulpbrongebeutenissen met datumterm bieden

### KActivities

- [kactivities] nieuw pictogram activiteiten gebruiken

### KArchive

- Aanmaken van archieven op Android-inhoud: URL's

### KCompletion

- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)

### KConfig

- Geheugenlek in KConfigWatcher repareren
- KCONFIG_USE_DBUS uitschakelen op Android

### KConfigWidgets

- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)
- [KColorSchemeManager] Voorbeeldgeneratie optimaliseren

### KCoreAddons

- KProcessInfo::name() geeft nu alleen de naam van het uitvoerbare programma. Voor de volledige opdrachtregel KProcessInfo::command() gebruiken

### KCrash

- Inschakelen van kcrash vermijden als het slechts ingevoegd is via een plug-in (bug 401637)
- kcrash uitschakelen bij uitvoeren onder rr

### KDBusAddons

- Race-conditie repareren op automatisch herstarten van kcrash

### KDeclarative

- Waarschuwen als KPackage ongeldig is
- [GridDelegate] Sselecteer geen niet geselecteerd item bij klikken op een van zijn actieknoppen (bug 404536)
- [ColorButton] Geaccepteerd signaal doorgeven uit ColorDialog
- op nul gebaseerd coördinatensysteem op de plot gebruiken

### KDesignerPlugin

- Keur af kgendesignerplugin, laat bundel-plug-in vallen voor alle KF5-widgets

### KDE WebKit

- ECMAddQtDesignerPlugin gebruiken in plaats van privé kopie

### KDocTools

- KF5DocToolsMacros.cmake: Niet afgekeurde KDEInstallDirs variabelen gebruiken (bug 410998)

### KFileMetaData

- Schrijven van afbeeldingen implementeren

### KHolidays

- Bestandsnaam tonen waar we een fout teruggeven

### KI18n

- Lange tekenreeksen met getallen lokaal weergeven (bug 409077)
- Doorgeven van doel aan ki18n_wrap_ui-macro ondersteunen

### KIconThemes

- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)

### KIO

- Naar prullenbak gestuurde bestanden op het bureaublad ongedaan maken is gerepareerd (bug 391606)
- kio_trash: copyOrMove opsplitsen, voor een betere foutmelding dan "zou nooit gebeuren"
- FileUndoManager: meer heldere toekenning bij vergeten om op te nemen
- Afsluiten en crash in kio_file repareren bij mislukken van put() in readData
- [CopyJob] Crash repareren bij kopiëren van een al bestaande map en drukken op "Overslaan" (bug 408350)
- [KUrlNavigator] MIME-types ondersteund door krarc naar isCompressedPath toevoegen (bug 386448)
- Toegevoegde dialoog om uitvoerrecht in te stellen voor uit te voeren bestand bij poging om deze te starten
- [KPropertiesDialog] Altijd aankoppelpunt controleren op null zijn (bug 411517)
- [KRun] MIME-type eerst controleren op isExecutableFile
- Een pictogram toevoegen voor de prullenbak startmap en een juist label (bug 392882)
- Ondersteuning voor behandelen van QNAM SSL fouten naar KSslErrorUiData toevoegen
- Zorgen dat FileJob zich consistent gedraagt
- [KFilePlacesView] asynchrone KIO::FileSystemFreeSpaceJob gebruiken
- interne 'kioslave' uitvoerbare helper hernoemen naar 'kioslave5' (bug 386859)
- [KDirOperator] zet puntjes in het midden voor labels die te lang zijn om te passen (bug 404955)
- [KDirOperator] nieuwe mappenopties volgen toevoegen
- KDirOperator: Alleen menu "Nieuwe aanmaken" inschakelen als het geselecteerde item een map is
- KIO FTP: hangen van bestand kopiëren repareren bij kopiëren naar een bestaand bestand (bug 409954)
- KIO: overzetten naar niet-afgekeurde KWindowSystem::setMainWindow
- Zorgen dat bladwijzernamen consistent worden opgeslagen
- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)
- [KDirOperator] meer mens-leesbare beschrijvingen van sorteervolgorde gebruiken
- [Rechtenbewerker] pictogrammen overzetten om QIcon::fromTheme() te gebruiken (bug 407662)

### Kirigami

- De aangepaste overflowknop vervangen door PrivateActionToolButton in ActionToolBar
- Als een submenu-actie een pictogram heeft ingesteld, ga na dat het ook getoond wordt
- [Separator] kleur van Breeze-randen overeen laten komen
- Kirigami ListSectionHeader-component toevoegen
- Contextmenuknop voor pagina's die niet verschijnen repareren
- PrivateActionToolButton met menu repareren die gecontroleerd status onjuist wist
- instellen van aangepast pictogram voor de linker hendel van de la toestaan
- De logica van visibleActions in SwipeListItem opnieuw bewerken
- Gebruik van QQC2 acties toestaan op Kirigami-componenten en maak nu K.Action gebaseerd op QQC2.Action
- Kirigami.Icon: laden van grotere afbeeldingen repareren wanneer bron een URL is (bug 400312)
- Pictogram gebruikt door Kirigami.AboutPage toevoegen

### KItemModels

- Q_PROPERTIES-interface toevoegen aan KDescendantsProxyModel
- Ga weg van verouderde methoden in Qt

### KItemViews

- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)

### KNotification

- Dubbele meldingen vermijden die getoond worden op Windows en witruimtes verwijderen
- Toepassingspictogram van 1024x1024 als terugvalpictogram in Snore hebben
- Parameter <code>-pid</code> toevoegen aan aanroepen van Snore-backend
- Snoretoast-backend toevoegen voor KNotifications op Windows

### KPeople

- Mogelijk maken om contactpersonen te verwijderen uit backends
- Mogelijk maken om contactpersonen te wijzigen

### KPlotting

- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)

### KRunner

- Nagaan dat we controleren of afsluiten gedaan moet worden na beëindigen van job
- Een gereedsignaal toevoegen aan FindMatchesJob in plaats van QObjectDecorator verkeerd gebruiken

### KTextEditor

- Aangepaste attributen voor KSyntaxHighligting-thema's toestaan
- Voor alle thema's repareren: sta uitzetten van attributen in XML accentueringsbestanden toe
- isAcceptableInput + toestaan alle zaken voor invoermethodes vereenvoudigen
- typeChars vereenvoudigen, geen noodzaak voor terugkeercode zonder filtering
- QInputControl::isAcceptableInput() nadoen bij filtering van ingetypte tekens (bug 389796)
- opschonen van regeleindes bij plakken proberen (bug 410951)
- Reparatie: afschakelen van attributen in XML accentueringsbestanden toestaan
- woordaanvulling verbeteren om accentuering te gebruiken om woordgrenzen te detecteren (bug 360340)
- Meer overzetten van QRegExp naar QRegularExpression
- op de juiste manier controleren of diff-commando gestart kan worden voor verschil bepalen in swap-bestand (bug 389639)
- KTextEditor: flikkering in linker rand repareren bij omschakelen tussen documenten
- Nog enige QRegExps naar QRegularExpression migreren
- 0 toestaan in reeksen regels in modus vim
- CMake find_dependency gebruiken in plaats van find_package in CMake configuratiesjabloonbestand

### KTextWidgets

- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)

### KUnitConversion

- Decibel energie-eenheden toevoegen (dBW en meervouden)

### KWallet Framework

- KWallet: opstarten van kwalletmanager repareren, de desktop-bestandsnaam heeft er een '5' in

### KWayland

- [server] Wrap proxyRemoveSurface in smart pointer
- [server] meer huidige modus in cache gebruiken en geldigheid toekennen
- [server] Huidige modus in de cache zetten
- zwp_linux_dmabuf_v1 implementeren

### KWidgetsAddons

- [KMessageWidget] widget aan standardIcon() doorgeven
- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)

### KWindowSystem

- KWindowSystem: cmake-optie KWINDOWSYSTEM_NO_WIDGETS toevoegen
- slideWindow(QWidget *widget) afkeuren
- KWindowSystem::setMainWindow(QWindow *) overload toevoegen
- KWindowSystem: setNewStartupId(QWindow *...) overload toevoegen

### KXMLGUI

- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)

### NetworkManagerQt

- WirelessDevice::lastRequestScanTime naar WirelessDevice::lastRequestScan hernoemen
- Eigenschap lastScanTime en lastRequestTime aan WirelessDevice toevoegen

### Plasma Framework

- Notificatiepictogrammen de stijl outline laten gebruiken
- bepalen van de grootte van de knoppen van hulpmiddelen meer coherent maken
- applets/containments/wallpaper toestaan om UIReadyConstraint uit te stellen
- pictogram voor meldingen laten lijken op bel (bug 384015)
- Onjuiste initiële tabpositie van verticale tabbalken repareren (bug 395390)

### Omschrijving

- Telegram Desktop plug-in in Fedora gerepareerd

### QQC2StyleBridge

- Slepen van inhoud in QQC2 ComboBox buiten menu voorkomen

### Solid

- Seriële eigenschap constant maken
- Technologie-eigenschap zichtbaar in batterij-interface

### Sonnet

- Optie om Qt Designer plug-in te bouwen toevoegen (BUILD_DESIGNERPLUGIN, standaard AAN)

### Accentuering van syntaxis

- C &amp; ISO C++: digraphs (token uit twee tekens) toevoegen (invouwen &amp; preprocessor) (bug 411508)
- Markdown, TypeScript &amp; Logcat: enige reparaties
- Formaatklassen: functies toevoegen om te weten of XML-bestanden stijlattributen instellen
- test.m zaken combineren in bestaande highlight.m
- Ondersteuning voor inheemse Matlab-tekenreeksen
- Gettext: stijl "Translated String" en attribuut spellChecking toevoegen (bug 392612)
- De OpenSCAD inspringen instellen naar C-stijl in plaats van geen
- Mogelijkheid om definitiegegevens te wijzigen na laden
- Accentueringsindexeerder: controleer kate-versie
- Markdown: meervoudige verbeteringen en reparaties (bug 390309)
- JSP: ondersteuning van &lt;script&gt; en &lt;style&gt; ; IncludeRule ##Java gebruiken (bug 345003)
- LESS: CSS-trefwoorden importeren, nieuwe accentuering en enige verbeteringen
- JavaScript: onnodige context "Conditional Expression" verwijderen
- Nieuwe syntaxis: SASS. Enige reparaties voor CSS en SCSS (bug 149313)
- CMake find_dependency gebruiken in CMake-configuratiebestand in plaats van find_package
- SCSS: interpolatie (#{...}) repareren en de interpolatiekleur toevoegen
- Attribuut van additionalDeliminator repareren (bug 399348)
- C++: contracten zijn niet in C++20
- Gettext: "vorige niet-vertaalde tekenreeks" repareren en andere verbeteringen/reparaties
- Jam: local repareren met variabele zonder initialisatie en SubRule accentueren
- impliciete fallthrough als er fallthroughContext is
- Algemene GLSL-bestandsextensies (.vs, .gs, .fs) toevoegen
- Latex: verschillende reparaties (math modus, geneste verbatim, ...) (bug 410477)
- Lua: kleur van einde met verschillende niveaus van voorwaarden en nesten van functies
- Accentueringsindexeerder: alle waarschuwingen zijn fataal

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
