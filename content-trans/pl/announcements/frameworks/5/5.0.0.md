---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE Ships First Release of Frameworks 5.
layout: framework
qtversion: 5.2
title: Pierwsze wydanie Szkieletów KDE 5
---
7 czerwiec 2014.  Społeczność KDE z dumą ogłasza wydanie Szkieletów KDE 5.0. Szkielety 5 są następną generacją bibliotek KDE, zmodularyzowanych i zoptymalizowanych pod kątem łatwości integracji z aplikacjami Qt. Szkielety dostarczają różnorodnych powszechnie potrzebnych możliwości w postaci dojrzałych i wypróbowanych bibliotek o przyjaznych warunkach licencji. Istnieje ponad 50 różnych Szkieletów tworzących to wydanie. Szkielety te zapewniają rozwiązania takie jak integracja sprzętu, obsługa formatu plików, dodatkowe elementy interfejsu, funkcje rysowania, sprawdzanie pisowni i więcej. Wiele ze Szkieletów jest między-platformowych i mają minimalne zależności lub nie mają ich w ogóle, czyniąc je łatwymi do zbudowania i dodania do każdej aplikacji Qt.

Szkielety KDE są wysiłkiem skierowanym na przebudowę bibliotek Platformy KDE 4 na zestaw niezależnych, między-platformowych modułów, które będą łatwo dostępne dla  wszystkich programistów Qt w celu uproszczenia, przyspieszenia i zmniejszenia kosztu programowania z Qt. Poszczególne Szkielety są między-platformowe, dobrze udokumentowane, wypróbowane, a ich używanie będzie znane programistom Qt, gdyż będą naśladować styl i standardy ustalone przez Projekt Qt. Szkielety są rozwijane według modelu zarządzania KDE z przewidywalnym planem wydań, przejrzystym i neutralnym względem firm procesie współtworzenia, otwartym zarządzaniem i elastycznym licencjonowaniem (LGPL).

The Frameworks have a clear dependency structure, divided into Categories and Tiers. The Categories refer to runtime dependencies:

- <strong>Functional</strong> elements have no runtime dependencies.
- <strong>Integration</strong> designates code that may require runtime dependencies for integration depending on what the OS or platform offers.
- <strong>Solutions</strong> have mandatory runtime dependencies.

The <strong>Tiers</strong> refer to compile-time dependencies on other Frameworks. Tier 1 Frameworks have no dependencies within Frameworks and only need Qt and other relevant libraries. Tier 2 Frameworks can depend only on Tier 1. Tier 3 Frameworks can depend on other Tier 3 Frameworks as well as Tier 2 and Tier 1.

Przejście z Platformy na Szkielety trwało przez 3 lata i było prowadzone przez najlepszych, od strony technicznej, współtwórców KDE.Dowiedz się więcej o Szkieletach 5 <a href='Szkieletów KDE %1'>z tego artykułu z ubiegłego roku</a>.

## Główne zmiany

Obecnie dostępnych jest ponad 50 Szkieletów. Całkowity ich zestaw można przeglądać <a href='Szkieletów KDE %1'>na stronach internetowych dokumentacji API</a>. Poniżej przedstawiono wrażenia z tego co może dostarczyć funkcjonalność Szkieletów programistom aplikacji Qt.

<strong>KArchive</strong> dostarcza obsługi dla wielu popularnych kodeków kompresji w zamkniętej w sobie, zaawansowanej i łatwej w użyciu bibliotece do pakowania i wypakowywania plików. Wystarczy dać jej pliki; nie ma potrzeby wymyślać na nowo funkcji pakowania w twoich aplikacjach opartych na Qt!

<strong>ThreadWeaver</strong> dostarcza wysokopoziomowe API do zarządzania wątkami przy użyciu interfejsów opartych na zadaniach i kolejkach. Umożliwia proste planowanie wykonywania wątku poprzez określanie zależności pomiędzy wątkami i wykonywanie ich po spełnieniu tych zależności, znacznie upraszczając użycie wielu wątków.

<strong>KConfig</strong> jest Szkieletem stworzonym do radzenia sobie z przechowywaniem i odczytywaniem ustawień. Zapewnia API nakierowane na grupy. Działa z plikami INI i zgodnymi z XDG katalogami kaskadowymi. Tworzy kod na podstawie plików XML.

<strong>Solid</strong> dostarcza możliwość wykrywania sprzętu, a ponadto dostarcza dane na temat urządzeń przechowywania, wolumenów, procesora, stanu baterii, zarządzania energią, stanu sieci i interfejsów oraz Bluetooth. Dla partycji szyfrowanych, zarządzania zasilaniem i siecią, wymagane jest uruchomienie odpowiednich usług.

<strong>KI18n</strong> dodaje obsługę Gettext dla aplikacji, czyniąc łatwiejszym przepływ pracy nad tłumaczeniami aplikacji Qt w ogólnej infrastrukturze tłumaczeń wielu projektów.
