---
aliases:
- ../announce-applications-16.04-beta
custom_spread_install: true
date: 2016-03-24
description: O KDE Lança as Aplicações do KDE 16.04 Beta.
layout: application
release: applications-16.03.80
title: O KDE disponibiliza a versão Beta do KDE Applications 16.04
---
24 de março de 2016. Hoje o KDE disponibilizou o beta da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Veja mais informações nas <a href='https://community.kde.org/Applications/16.04_Release_Notes'>notas de lançamento da comunidade</a> sobre novos pacotes, pacotes que sejam agora baseados no KF5 e problemas conhecidos. Será disponibilizado um anúncio mais completo para a versão final

Com diversos aplicativos sendo preparados para o KDE Frameworks 5, as versões do KDE Applications 16.04 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do usuário. Precisamos dos usuários atuais para manter a alta qualidade do KDE, porque os desenvolvedores simplesmente não conseguem testar todas as configurações possíveis. Contamos com você para nos ajudar a encontrar erros antecipadamente, para que possam ser corrigidos antes da versão final. Por favor, considere juntar-se à equipe, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.

#### Instalando os pacotes binários do KDE Applications 16.04 Beta

Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários das Aplicações do KDE 16.04 Beta (internamente 16.03.80) para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram. Os pacotes binários adicionais, assim como as actualizações dos pacotes agora disponíveis, poderão aparecer nas próximas semanas.

<em>Localizações dos Pacotes</em>. Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki da Comunidade</a>.

#### Compilando o KDE Applications 16.04 Beta

Poderá <a href='http://download.kde.org/unstable/applications/16.03.80/src/'>transferir à vontade</a> o código-fonte completo do 16.04 Beta. As instruções de compilação e instalação estão disponíveis na <a href=/info/applications/applications-16.03.80'>Página de Informações das Aplicações do KDE Beta</a>.
