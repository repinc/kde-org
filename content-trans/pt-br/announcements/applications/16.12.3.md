---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: O KDE disponibiliza o KDE Applications 16.12.3
layout: application
title: O KDE disponibiliza o KDE Applications 16.12.3
version: 16.12.3
---
9 de Março de 2017. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../16.12.0'>Aplicações do KDE 16.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Mais de 20 correções de erros registradas incluem melhorias no kdepim, ark, filelight, gwenview, kate, kdenlive, okular, entre outros.

Esta versão também inclui as versões de Suporte de Longo Prazo da Plataforma de Desenvolvimento do KDE 4.14.30.
