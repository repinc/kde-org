---
aliases:
- ../../kde-frameworks-5.19.0
date: 2016-02-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Simplificação da pesquisa e inicialização dos 'plugins' do Attica

### Ícones do Brisa

- Muitos ícones novos
- Adição dos ícones de tipos MIME em falta do conjunto de ícones Oxygen

### Módulos Extra do CMake

- ECMAddAppIcon: Usar localizações absolutas ao lidar com os ícones
- Certificação que o prefixo é pesquisado no Android
- Adição de um módulo FindPoppler
- Uso dos PATH_SUFFIXES no ecm_find_package_handle_library_components()

### KActivities

- Não invocar o exec() a partir do QML (erro 357435)
- A biblioteca KActivitiesStats está agora num repositório separado

### KAuth

- Execução também do preAuthAction nos Backends com AuthorizeFromHelperCapability
- Correcção do nome do serviço de D-Bus do agente do PolKit

### KCMUtils

- Correcção de problema com o HiDPI no KCMUtils

### KCompletion

- O método KLineEdit::setUrlDropsEnabled não pode ser marcado como obsoleto

### KConfigWidgets

- adição de um esquema de cores "Complementar" ao 'kcolorscheme'

### KCrash

- Actualização da documentação do KCrash::initialize. Os programadores das aplicações são encorajados a chamá-la de forma explícita.

### KDeclarative

- Limpeza das dependências do KDeclarative/QuickAddons
- [KWindowSystemProxy] Adição de método de modificação do 'showingDesktop'
- DropArea: Correcção da omissão do evento 'dragEnter' com o 'preventStealing'
- DragArea: Implementação de um item delegado de captura
- DragDropEvent: Adição da função ignore()

### KDED

- Reversão do truque do BlockingQueuedConnection, porque o Qt 5.6 irá ter uma correcção melhor
- Fazer com que o 'kded' se registe com nomes alternativos definidos pelos módulos do 'kded'

### Suporte para a KDELibs 4

- Dependência para o 'kdelibs4support' do 'kded' (para o 'kdedmodule.desktop')

### KFileMetaData

- Permissão da pesquisa do URL de origem de um ficheiro

### KGlobalAccel

- Evitar um estoiro no caso de o D-Bus não ficar disponível

### Extensões da GUI do KDE

- Correcção da listagem das paletas disponíveis na janela de cores

### KHTML

- Correcção da detecção do tipo de ligação do ícone ("favicon")

### KI18n

- Redução do uso da API do Gettext

### KImageFormats

- Adição dos 'plugins' de E/S de imagens para 'kra' e 'ora' (apenas para leitura)

### KInit

- Ignorar a área de visualização do ecrã actual na informação de arranque
- Migração do 'klauncher' para o 'xcb'
- Uso do 'xcb' na interacção com o KStartupInfo

### KIO

- Nova classe FavIconRequestJob na nova biblioteca KIOGui, para a obtenção de 'favicons'
- Correcção do estoiro do KDirListerCache com duas listagens para uma pasta vazia na 'cache' (erro 278431)
- Mudança da implementação em Windows do KIO::stat para o protocolo file:/ dar um erro se o ficheiro não existir
- Não assumir que os ficheiros apenas para leitura não podem ser apagados no Windows
- Correcção do ficheiro .pri do KIOWidgets: depende do KIOCore, não de si própria
- Correcção do auto-carregamento do 'kcookiejar', dado que os valores foram trocados no '6db255388532a4'
- Disponibilização do 'kcookiejar' no nome de serviço D-Bus 'org.kde.kcookiejar5'
- kssld: instalação do ficheiro do serviço D-Bus para o 'org.kde.kssld5'
- Oferta de um ficheiro de serviço de D-Bus para o org.kde.kpasswdserver
- [kio_ftp] correcção da apresentação da data/hora de modificação dos ficheiros/pastas (erro 354597)
- [kio_help] correcção do envio de lixo ao servir ficheiros estáticos
- [kio_http] Tentativa da autenticação em NTLMv2 se o servidor recusar o NTLMv1
- [kio_http] correcção de erros de migração que danificaram a 'cache'
- [kio_http] Correcção da criação da resposta de nível 3 do NTLMv2
- [kio_http] Correcção da espera até a limpeza de 'cache' atender no 'socket'
- kio_http_cache_cleaner: não sair no arranque se a pasta da 'cache' ainda não existir
- Mudança do nome de D-Bus do kio_http_cache_cleaner para que não saia se a versão para o 'kde4' ainda estiver a correr

### KItemModels

- KRecursiveFilterProxyModel::match: Correcção de um estoiro

### KJobWidgets

- Correcção de estoiros nas janelas do KJob (erro 346215)

### Plataforma de Pacotes

- Evitar encontrar o mesmo pacote várias vezes a partir de locais diferentes

### KParts

- PartManager: parar de seguir um elemento, mesmo que já não esteja no nível de topo (erro 355711)

### KTextEditor

- Melhor comportamento na funcionalidade de envolvência de parêntesis automáticos
- Mudança da chave de opção para forçar uma nova predefinição, Fim de Linha se Fim do Ficheiro = verdadeiro
- Remoção de algumas chamadas suspeitas ao 'setUpdatesEnabled' (erro 353088)
- Atraso na emissão do 'verticalScrollPositionChanged' até que tudo esteja consistente na dobragem (erro 342512)
- Correcção da actualização da substituição de marcas (erro 330634)
- Actualização apenas uma vez da paleta para o evento de mudança que pertence à qApp (erro 358526)
- Adição de mudanças de linha no EOF por omissão
- Adição do realce de sintaxe para o NSIS

### Plataforma da KWallet

- Duplicação do descritor de ficheiros ao abrir o ficheiro para ler o ambiente

### KWidgetsAddons

- Correcção dos elementos dependentes que lidam com o KFontRequester
- KNewPasswordDialog: uso do KMessageWidget
- Protecção contra estoiro-à-saída no KSelectAction::~KSelectAction

### KWindowSystem

- Mudança do cabeçalho da licença de "Library GPL 2 or later" para "Lesser GPL 2.1 or later" (da versão mínima 2 para a 2.1)
- Correcção de estoiro se o KWindowSystem::mapViewport for chamado sem uma QCoreApplication
- 'Cache' da QX11Info::appRootWindow no 'eventFilter' (erro 356479)
- Remoção da dependência do QApplication (erro 354811)

### KXMLGUI

- Adição de opção para desactivar o KGlobalAccel na altura da compilação
- Reparação do esquema de atalhos da localização para a aplicação
- Correcção da listagem de ficheiros de atalhos (uso inválido do QDir)

### NetworkManagerQt

- Nova verificação do estado da ligação e de outras propriedades para se certificar que estão actualizadas (versão 2) (erro 352326)

### Ícones do Oxygen

- Remoção de ficheiros com ligações quebradas
- Adição de ícones aplicacionais das aplicações do KDE
- Adição de ícones de locais do Brisa ao Oxygen
- Sincronização dos ícones de tipos MIME do Oxygen com o Brisa

### Plataforma do Plasma

- Adição de uma propriedade 'separatorVisible'
- Remoção mais explícita do m_appletInterfaces (erro 358551)
- Uso do 'complementaryColorScheme' do KColorScheme
- AppletQuickItem: Não tentar definir um tamanho inicial maior que o tamanho do item-pai (erro 358200)
- IconItem: Adição da propriedade 'usesPlasmaTheme'
- Não carregar a área de ferramentas nos tipos diferentes de 'desktop' ou 'panel'
- IconItem: Tentar o carregamento dos ícones do QIcon::fromTheme como SVG (erro 353358)
- Ignorar a verificação, caso apenas uma parte do ficheiro seja zero no 'compactRepresentationCheck' (erro 358039)
- [Unidades] Devolver pelo menos 1ms nas durações (erro 357532)
- Adição do clearActions() para remover todas as acções de interface das 'applets'
- [plasmaquick/dialog] Não usar o KWindowEffects para o tipo de janela Notification
- O Applet::loadPlasmoid() é agora obsoleto
- [PlasmaCore DataModel] Não reiniciar o modelo quando é removida uma origem
- Correcção das sugestões de margem nos SVG's de fundo dos painéis opacos
- IconItem: Adição da propriedade 'animated'
- [Unity] Escala do tamanho de ícones do ambiente de trabalho
- o botão é composto-nos-contornos
- paintedWidth/paintedheight no IconItem

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
