---
aliases:
- ../../kde-frameworks-5.43.0
date: 2018-02-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nya moduler

KHolidays: Bibliotek för helgdagsberäkning

Biblioteket tillhandahåller ett C++ programmeringsgränssnitt som avgör helgdagar och andra speciella evenemang för ett geografiskt område.

Syfte: Erbjuder tillgängliga åtgärder för ett specifikt syfte

Ramverket erbjuder möjlighet att skapa integrerade tjänster och åtgärder för vilket program som helst utan att behöva implementera dem specifikt. Purpose erbjuder dem mekanismer för att lista de olika alternativen att köra given den begärda åtgärdstypen och underlättar komponenter så att alla insticksprogram kan ta emot all information de behöver.

### Baloo

- balooctl status: Skapa tolkningsbar utdata
- Rätta djupa kopior av I/O-slav etiketterade kataloger. Det gör att listning av etiketterade kataloger i etiketträdet går sönder, men det är bättre en söndriga kopior.
- Hoppa över att placera nya icke indexerade filer i kö och ta omedelbart bort dem från indexet
- Ta bort nya icke indexerade flyttade filer från indexet

### Breeze-ikoner

- Lägg till saknade Krusader-ikoner för katalogsynkronisering (fel 379638)
- Uppdatera ikon för listborttagning med - istället ikon för avbryt (fel 382650)
- Lägg till ikoner för pulsaudio Plasmoid (fel 385294)
- Använd samma ogenomskinlighet 0,5 överallt
- Ny ikon för virtualbox (fel 384357)
- Gör väder-dimma natt- och dagneutral (fel 388865)
- Installera verkligen det ny animeringssammanhanget
- QML-fil Mime ser nu likadan ut i alla storlekar (fel 376757)
- Uppdatera animeringsikoner (fel 368833)
- Lägg till emblem-delad färgad ikon
- Rätta felaktiga index.theme filer, "Context=Status" saknades i status/64
- Ta bort 'körbar' rättigheter från .svg-filer
- Åtgärdsikon för nerladdning är länkad till redigera-nerladdning (fel 382935)
- Uppdatera ikontema för Dropbox ikon för systembrickan (fel 383477)
- Saknar emblem-förval-symbolisk (fel 382234)
- Typ i Mime-typ filnamn (fel 386144)
- Använd en mer specifik ikon för octave (fel 385048)
- Lägg till valvikoner (fel 386587)
- Skala bildpunkt statusikoner (fel 386895)

### Extra CMake-moduler

- FindQtWaylandScanner.cmake: Använd qmake-query som TIPS
- Säkerställ att sökning efter Qt5-baserad qmlplugindump görs
- ECMToolchainAndroidTest finns inte längre (fel 389519)
- Ställ inte in LD_LIBRARY_PATH i prefix.sh
- Lägg till FindSeccomp i find-modules
- Återgå till språknamn för översättningsuppslagning om landsnamnet misslyckas
- Android: Lägg till fler deklarationsfiler

### KAuth

- Rätta länkningsregression introducerad i 5.42.

### KCMUtils

- Lägg till verktygstips för de två knapparna på varje post

### KCompletion

- Rätta felaktig utsändning av textEdited() av KLineEdit (fel 373004)

### KConfig

- Använd Ctrl+Shift+, som standardgenväg för "Anpassa &lt;program&gt;"

### KCoreAddons

- Matcha också spdx-nycklarna LGPL-2.1 &amp; LGPL-2.1+
- Använd den mycket snabbar metoden urls() från QMimeData (fel 342056)
- Optimera inotify KDirWatch-gränssnitt: avbilda inotify wd till Entry
- Optimera: använd QMetaObject::invokeMethod med functor

### KDeclarative

- [ConfigModule] Återanvänd QML-sammanhang och gränssnitt om något finns (fel 388766)
- [ConfigPropertyMap] Lägg till saknad deklarationsfil
- [ConfigPropertyMap] Skicka inte ut valueChanged vid ursprunglig skapelse

### KDED

- Exportera inte kded5 som ett CMake-mål

### Stöd för KDELibs 4

- Strukturera om Solid::NetworkingPrivate till en delad och plattformsspecifik implementering
- Rätta kompileringsfel för mingw "src/kdeui/kapplication_win.cpp:212:22: error: 'kill' was not declared in this scope"
- Rätta kded D-Bus-namn i solid-networking handledning

### KDesignerPlugin

- Gör beroende av kdoctools valfritt

### KDESU

- Gör det möjligt att bygga med KDESU_USE_SUDO_DEFAULT igen
- Gör så att kdesu fungerar när PWD är /usr/bin

### KGlobalAccel

- Använd cmake-funktionen 'kdbusaddons_generate_dbus_service_file' från kdbusaddons för att generera tjänstfil för D-bus (fel 382460)

### KDE GUI Addons

- Rätta länkningav skapad QCH-fil till QtGui dokumentation

### KI18n

- Rätta att libintl hittas vid "korskompilering" av inbyggda Yocto-paket

### KInit

- Rätta korskompilering med MinGW (MXE)

### KIO

- Reparera kopiering av fil till VFAT utan varningar
- kio_file: Hoppa över felhantering för ursprungliga rättigheter vid filkopiering
- kio_ftp: Skicka inte ut felsignal innan alla listkommandon har provats (fel 387634)
- Prestanda: Använd KFileItem målobjektet för att räkna ut om det är skrivbart istället för att skapa en KFileItemListProperties
- Prestanda: Använd KFileItemListProperties kopieringskonstruktor istället för konvertering från KFileItemList till KFileItemListProperties. Det sparar utvärdering av alla objekt igen.
- Förbättra felhantering i I/O-slaven för filer
- Ta bort jobbflaggan PrivilegeExecution
- KRun: Tillåt att utföra "lägg till nätverkskatalog" utan bekräftelsefråga
- Tillåt att filtrera platser baserade på alternativt programnamn
- [Sökleverantör för webbadressfilter] Undvik dubbel borttagning (fel 388983)
- Rätta överlappning av första objektet i KFilePlacesView
- Inaktivera tillfälligt stöd för KAuth i KIO
- previewtest: Tillåt att aktiverade insticksprogram anges
- [KFileItem] Använd "emblem-shared" för delade filer
- [DropJob] Aktivera drag och släpp i en skrivskyddad katalog
- [FileUndoManager] Aktivera att ångra ändringar i skrivskyddade kataloger
- Lägg till stöd för privilegierad körning i KIO-jobb (tillfälligt inaktiverad i den här utgåvan)
- Lägg till stöd för att dela fildeskriptorer mellan KIO-slav för filer och dess KAuth hjälpobjekt
- Rätta KFilePreviewGenerator::LayoutBlocker (fel 352776)
- KonqPopupMenu/Plugin kan nu använda nyckeln X-KDE-RequiredNumberOfUrls för att kräva att ett visst antal filer markeras innan de visas
- [KPropertiesDialog] Aktivera radbrytning för beskrivning av checksumma
- Använd cmake-funktionen 'kdbusaddons_generate_dbus_service_file' från kdbusaddons för att generera tjänstfil för D-bus (fel 388063)

### Kirigami

- Stöd för färggrupper
- Ingen återmatning vid klick om objektet inte vill ha mushändelser
- Provisorisk lösning för program som använder listitems felaktigt
- Utrymme för rullningslisten (fel 389602)
- Tillhandahåll ett verktygstips för huvudåtgärden
- cmake: Använd den officiella CMake-variabeln för att bygga som ett statiskt insticksprogram
- Uppdatera läsbar lagerbenämning i dokumentation av programmeringsgränssnitt
- [ScrollView] Rulla en sida med skift+mushjul
- [PageRow] Navigera mellan nivåer med musens bakåt- och framåtknappar
- Säkerställ att skrivbordsikon ritas med rätt proportion (fel 388737)

### KItemModels

- KRearrangeColumnsProxyModel: Rätta krasch när det inte finns någon källmodell
- KRearrangeColumnsProxyModel: Implementera om sibling() så att det fungerar som förväntat

### KJobWidgets

- Ta bort duplicerad kod i byteSize(double size) (fel 384561)

### KJS

- Gör beroende av kdoctools valfritt

### KJSEmbed

- Exportera inte kjscmd
- Gör beroende av kdoctools valfritt

### KNotification

- Underrättelseåtgärden "Kör kommando" har rättats (fel 389284)

### KTextEditor

- Rättning: Visa hopp när rulla förbi dokumentets slut är aktiverat (fel 306745)
- Använd minst begärd bredd för trädet med argumenttips
- ExpandingWidgetModel: Hitta kolumnen längst till höger baserat på plats

### KWidgetsAddons

- KDateComboBox: Rätta att dateChanged() inte skickas ut efter att ett datum skrivits in (fel 364200)
- KMultiTabBar: Rätta regression i konvertering till ny version av connect()

### Plasma ramverk

- Definiera egenskap för Plasma-stilarna i Units.qml
- windowthumbnail: Rätta urvalskoden av GLXFBConfig
- [Standardverktygstips] Rätta storleksändring (fel 389371)
- [Plasma-dialogruta] Anropa bara fönstereffekter om synlig
- Rätta en källa till loggskräp refererad till i fel 388389 (tomt filnamn skickat till funktion)
- [Kalender] Justera kalenderns verktygsgrepp
- [ConfigModel] Ange QML-sammanhang för ConfigModule (fel 388766)
- [Ikonobjekt] Hantera källkod som börjar med ett snedstreck som lokal fil
- Rätta utseende för höger-till-vänster RTL i ComboBox (fel 387558)

### QQC2StyleBridge

- Lägg till BusyIndicator i listan över kontroller med stil
- Ta bort flimmer när musen hålls över rullningslist

### Solid

- [UDisks] Ignorera bara stödfil som inte hör till användaren om den är känd (fel 389358)
- Lagringsenheter monterade utanför /media, /run/media och $HOME ignoreras nu, samt loop-enheter vars (fel 319998)
- [UDisks enhet] Visa loop-enhet med stödfilnamn och ikon

### Sonnet

- Hitta Aspell-ordlistor på Windows

### Syntaxfärgläggning

- Rätta C# var reguljärt uttryck
- Stöd för understreck i numeriska litteraler (Python 3.6) (fel 385422)
- Färglägg Khronos Collada och glTF-filer
- Rätta färgläggning av ini-värden som innehåller tecknen : eller #
- AppArmor: nya nyckelord, förbättringar och rättningar

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
