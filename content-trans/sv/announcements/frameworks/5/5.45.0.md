---
aliases:
- ../../kde-frameworks-5.45.0
date: 2018-04-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Ställ explicit in innehållstyp till formulärdata

### Baloo

- Förenkla term operator &amp;&amp; och ||
- Hämta inte dokument-id för resultatposter som hoppas över
- Hämta inte mtime upprepade gånger från databas under sortering
- Exportera inte normalt databasesanitizer
- baloodb: Lägg till experimentellt meddelande
- Introducera baloodb kommandoradsverktyg
- Introducera klassen sanitizer
- [FileIndexerConfig] Fördröj ifyllnad av kataloger tills de verkligen används
- src/kioslaves/search/CMakeLists.txt - länk till Qt5Network efter ändringar av kio
- balooctl: checkDb ska också verifiera senast kända webbadress för documentId
- balooctl monitor: Återuppta för att vänta på tjänst

### Breeze-ikoner

- Lägg till window-pin ikon (fel 385170 lägg till window-pin ikon)
- Byt namn på 64 bildpunkters ikon tillagd för elisa
- Ändra 32 bildpunkters i ikon för blanda och upprepa spellista
- Saknade ikoner för meddelanden på plats (fel 392391)
- Ny ikon för Elisa musikspelare
- Lägg till mediastatusikoner
- Ta bor ram omkring mediaåtgärdsikoner
- Lägg till ikoner media-playlist-append och play
- Lägg till view-media-album-cover för babe

### Extra CMake-moduler

- Dra nytta av CMake uppströms infrastruktur för att detektera kompilatorverktygskedja
- Dokumentation av programmeringsgränssnitt: Rätta några "kodblocksrader" så att de har tomma rader före och efter
- Lägg till ECMSetupQtPluginMacroNames
- Tillhandahåll androiddeployqt med alla prefixsökvägar
- Inkludera "stdcpp-path" i json-filen
- Lös upp symboliska länkar i QML-importsökvägar
- Tillhandahåll QML-importsökvägar för androiddeployqt

### Integrering med ramverk

- kpackage-install-handlers/kns/CMakeLists.txt - Länk till Qt::Xml efter ändringar av knewstuff

### KActivitiesStats

- Anta inte att SQLite fungerar och avsluta inte vid fel

### KDE Doxygen-verktyg

- Sök först efter qhelpgenerator-qt5 för hjälpgenerering

### KArchive

- karchive, kzip: Försök att hantera duplicerade filer på ett något snyggare sätt
- Använd nullptr för att skicka en null-pekare till crc32

### KCMUtils

- Gör det möjligt att begära en inställningsmodul för ett insticksprogram programmatiskt
- Använd X-KDE-ServiceTypes konsekvent istället för ServiceTypes
- Lägg till X-KDE-OnlyShowOnQtPlatforms i definitionen av KCModule-tjänsttyp

### KCoreAddons

- KTextToHTML: Returnera när webbadressen är tom
- Städa m_inotify_wd_to_entry innan invalidering av Entry-pekare (fel 390214)

### KDeclarative

- Ställ bara in QQmlEngine en gång i QmlObject

### KDED

- Lägg till X-KDE-OnlyShowOnQtPlatforms i definitionen av KDEDModule-tjänsttyp

### KDocTools

- Lägg till poster för Elisa, Markdown, KParts, DOT, SVG i general.entities
- customization/ru: Rätta översättning av underCCBYSA4.docbook och underFDL.docbook
- Rätta dubblett av lgpl-notice/gpl-notice/fdl-notice
- customization/ru: Översätt fdl-notice.docbook
- Ändra stavning av kwave på begäran av underhållsansvarig

### KFileMetaData

- taglibextractor: Omstrukturera för bättre läsbarhet

### KGlobalAccel

- Orsaka inte assert vid felaktig användning från D-Bus (fel 389375)

### KHolidays

- holidays/plan2/holiday_in_en-gb - Uppdatera helgfil för Indien (fel 392503)
- Paketet skulle inte uppdateras. Kanske ett problem med skript
- Omarbetade helgfilen för Tyskland (fel 373686)
- Formatera README.md som verktygen förväntar sig (med ett introduktionsavsnitt)

### KHTML

- Undvik att fråga efter ett tomt protokoll

### KI18n

- Säkerställ att ki18n kan bygga sina egna översättningar
- Anropa inte PythonInterp.cmake i KF5I18NMacros
- Gör det möjligt att generera po-filer i parallell
- Skapa en konstruktor för KLocalizedStringPrivate

### KIconThemes

- Gör export av KIconEngine kommentarnoggrann
- Undvik asan fel vid körning

### KInit

- Ta bort IdleSlave som har tillfällig behörighet

### KIO

- Säkerställ att modellen är satt när resetResizing anropas
- pwd.h finns inte på Windows
- Ta bort normalt bort Senast sparade den här månaden, och Senast sparade förra månaden
- Låt KIO byggas för Android
- Inaktivera tillfälligt installation av filen I/O-slave kauth hjälp- och policyfil
- Hantera bekräftelsefrågor för privilegierade åtgärder i SlaveBase istället för i KIO::Job
- Förbättra följdriktighet för användargränssnittet "Öppna med" genom att alltid visa topprogrammet på plats
- Rätta krasch när enhet skickar läsning klar efter att jobbet är slutfört
- Markera valda objekt när överliggande katalog visas från dialogrutan öppna eller spara (fel 392330)
- Stöd dolda NTFS-filer
- Använd X-KDE-ServiceTypes konsekvent istället för ServiceTypes
- Rätta assert i concatPaths när en fullständig sökväg klistras in i radeditorn i KFileWidget
- [KPropertiesDialog] Stöd fliken Checksumma för alla lokala sökvägar (fel 392100)
- [KFilePlacesView] Anropa bara KDiskFreeSpaceInfo om nödvändigt
- FileUndoManager: Ta inte bort icke-existerande lokala filer
- [KProtocolInfoFactory] Rensa inte cachen om den just har byggts
- Försök inte heller hitta en ikon för en relativ webbadress (t.ex. '~')
- Använd riktig objektwebbadress för Skapa ny sammanhangsberoende meny (fel 387387)
- Rätta flera fall av felaktiga parametrar för findProtocol
- KUrlCompletion: Returnera tidigt om webbadressen är ogiltig som ":/"
- Försök inte hitta en ikon för en tom webbadress

### Kirigami

- Större ikoner i mobilläge
- Tvinga en innehållsstorlek in i bakgrundsstilobjektet
- Lägg till typen InlineMessage och exempelsida för galleriprogram
- Bättre heuristisk selektiv färgläggning
- Gör så att inläsning från lokala SVG:er faktiskt fungerar
- Stöd också ikoninläsningsmetoden i Android
- Använd en färgläggningsstrategi som liknar de föregående olika stilarna
- [Card] Använd egen implementering av "findIndex"
- Döda nätverksöverföringar om vi ändrar ikon under körning
- Första prototyp av delegerad återvinning
- Tillåt klienter av OverlaySheet att utelämna den inbyggda stängningsknappen
- Komponenter för Cards
- Rätta storlek på ActionButton
- Gör så att passiveNotifications visas längre, så att användare faktiskt kan läsa dem
- Ta bort beroende av oanvänd QQC1
- ToolbarApplicationHeader layout
- Gör det möjligt att visa titeln trots att ctx-åtgärder finns

### KNewStuff

- Rösta verkligen vid klick på stjärnor i listvyn (fel 391112)

### Ramverket KPackage

- Försök att rätta FreeBSD-byggning
- Använd Qt5::rcc istället för att söka efter körbart program
- Använd NO_DEFAULT_PATH för att säkerställa att rätt kommando väljes
- Sök också efter körbara filer med prefix rcc
- Ställ in komponenter för riktig generering av qrc
- Rätta generering av rcc-binärpaket
- Skapa rcc-filen varje gång vid installation
- Gör så att org.kde.-komponenter inkluderar en bidragswebbadress
- Markera att kpackage_install_package ej avråds från för plasma_install_package

### KPeople

- Exponera PersonData::phoneNumber för QML

### Kross

- Inget behov av att kräva kdoctools

### KService

- Dokumentation av programmeringsgränssnitt: Använd X-KDE-ServiceTypes konsekvent istället för ServiceTypes

### KTextEditor

- Gör det möjligt för KTextEditor att byggas på Android NDK:s gcc 4.9
- Undvik Asan fel vid körning: skiftexponent -1 är negativ
- Optimering av TextLineData::attribute
- Beräkna inte attribute() två gånger
- Återställ rättning: Rättning: Visa hopp när rulla förbi dokumentets slut är aktiverat (fel 391838)
- Skräpa inte ner klippbordshistoriken med dubbletter

### Kwayland

- Lägg till gränssnitt för fjärråtkomst till KWayland
- [server] Lägg till stöds för ramsemantik för Pointer version 5 (fel 389189)

### KWidgetsAddons

- KColorButtonTest: Ta bort att-göra kod
- ktooltipwidget: Subtrahera marginaler från tillgänglig storlek
- [KAcceleratorManager] Ange bara iconText() om den verkligen ändrats (fel 391002)
- ktooltipwidget: Förhindra visning utanför skärmen
- KCapacityBar: Ställ in tillstånd QStyle::State_Horizontal
- Synkronisera med ändring av KColorScheme
- ktooltipwidget: Rätta positionering av verktygstips (fel 388583)

### KWindowSystem

- Lägg till "SkipSwitcher" i programmeringsgränssnitt
- [xcb] Rätta implementering av _NET_WM_FULLSCREEN_MONITORS (fel 391960)
- Reducera frystid för plasmashell

### ModemManagerQt

- cmake: Markera inte libnm-util som hittad när ModemManager hittas

### NetworkManagerQt

- Exportera deklarationskataloger för NetworkManager
- Börja kräva NM 1.0.0
- device: Definiera StateChangeReason och MeteredStatus som Q_ENUM
- Rätta konvertering av AccessPoint-flaggor till capabilities

### Plasma ramverk

- Skrivbordsunderläggsmallar: Ange bakgrundsfärg för att säkerställa kontrast att sampla textinnehåll
- Lägg till mall för Plasma skrivbordsunderlägg med QML-filändelse
- [ToolTipArea] Lägg till signalen "aboutToShow"
- windowthumbnail: Använd gammariktig skalning
- windowthumbnail: Använd mipmap strukturfiltrering (fel 390457)
- Ta bort oanvända X-Plasma-RemoteLocation poster
- Mallar: Ta bort X-Plasma-DefaultSize från miniprogrammetadata
- Använd X-KDE-ServiceTypes konsekvent istället för ServiceTypes
- Mallar: Ta bort oanvända X-Plasma-Requires-* poster från miniprogrammetadata
- Ta bort objektgrepp i en layout
- Reducera frystid för plasmashell
- Läs bara in i förväg efter omgivningen skickat uiReadyChanged
- Rätta söndrig kombinationsruta (fel 392026)
- Rätta textskalning med skalfaktorer som inte är heltal när PLASMA_USE_QT_SCALING=1 är inställt (fel 356446)
- Nya ikoner för nerkopplade och inaktiverade enheter
- [Dialog] Tillåt att ställa in outputOnly för dialogrutan NoBackground
- [ToolTip] Kontrollera filnamn i KDirWatch hanterare
- Inaktivera varning i kpackage_install_package som avråder från användning för tillfället
- [Breeze Plasma Theme] Använd currentColorFix.sh för att ändra medieikoner
- [Breeze Plasma Theme] Lägg till mediestatusikoner med cirklar
- Ta bor ramar omkring mediaknappar
- [Window Thumbnail] Tillåt användning av atlasstruktur
- [Dialog] Ta bort nu föråldrade anrop till KWindowSystem::setState
- Stöd Atlasstrukturer i FadingNode
- Rätta FadingMaterial-fragment med kärnprofil

### QQC2StyleBridge

- Rätta återgivning när inaktiverad
- Bättre layout
- Experimentellt stöd för automatiska minnesregler
- Säkerställ att vi tar hänsyn till storleken på elementet vid stiländring
- Rätta återgivning av teckensnitt för skärmar utan hög upplösning och heltalsskalfaktorer (fel 391780)
- Rätta ikonfärger med colorsets
- Rätta ikonfärger för verktygsknappar

### Solid

- Solid kan nu fråga efter batterier i t.ex. trådlösa spelkonsoler och styrspakar
- Använd nyligen introducerade UP uppräkningsvärden
- Lägg till spelinmatningsenheter och andra till Batteri
- Lägg till uppräkningsvärden för batterienheter
- [UDevManager] Fråga också explicit efter kameror
- [UDevManager] Filtrera redan för delsystem innan fråga (fel 391738)

### Sonnet

- Tvinga inte användning av standardklienten, välj en som stöder begärt språk.
- Inkludera ersättningssträngar i förslagslistan
- Implementera NSSpellCheckerDict::addPersonal()
- NSSpellCheckerDict::suggest() returnerar en lista med förslag
- Initiera språk för NSSpellChecker i konstruktorn NSSpellCheckerDict
- Implement loggningskategorin NSSpellChecker
- NSSpellChecker kräver AppKit
- Flytta NSSpellCheckerClient::reliability() ur linjen
- Använd föredragen Mac-plattformssymbol
- Använd rätt katalog för att slå upp trigram i Windows byggkatalog

### Syntaxfärgläggning

- Gör det möjligt att bygga projektet fullständigt med korskompilering
- Omkonstruktion av CMake syntaxgenerering
- Optimera syntaxfärgläggning av Bash, Cisco, Clipper, Coffee, Gap, Haml, Haskell
- Lägg till syntaxfärgläggning för MIB-filer

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
