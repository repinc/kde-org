---
aliases:
- ../4.3
- ../4.3.sl
date: '2009-08-04'
title: Najava izida KDE 4.3.0 Caizen
---

<h3 align="center">
  Skupnost KDE prinaša postopne inovacije z izidom novega KDE 4.3
</h3>

<p align="justify">
  <strong>
    KDE 4.3 (kodno ime: <i>»Caizen«</i>) uporabnikom prostega namizja in razvijalcem programske opreme prinaša postopne inovacije 
  </strong>
</p>

<p align="justify">
  4. avgust, 2009. <a href="/">Skupnost KDE</a> danes najavlja takojšnjo razpoložljivost KDE 4.3 <i>»Caizen«</i>, ki prinaša mnogo izboljšav uporabniškega doživetja in razvojnega okolja. KDE 4.3 še izboljša edinstvene zmožnosti, ki so jih prinesle predhodne izdaje, obenem pa prinaša nove inovacije. Medtem ko je bila izdaja 4.2 namenjena večini končnih uporabnikov, KDE 4.3 ponuja stabilen in kompleten izdelek za dom in malo pisarno.
</p>
<p align=justify>
  Skupnost KDE je v zadnjih 6 mesecih <strong>vključila skoraj 2.000 želenih zmožnosti</strong> in <strong>odpravila več kot 10.000 napak</strong>. Skoraj 700 prostovoljnih razvijalcev je opravilo blizu 63.000 sprememb. V nadaljevanju si preberite pregled sprememb v namiznem delovnem okolju, paketu programov in razvojnem okolju v KDE 4.3.
</p>

<div class="text-center">
	<a href="/announcements/4/4.3.0/images/kde430-desktop.png">
	<img src="/announcements/4/4.3.0/images/kde430-desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Namizno okolje v KDE 4.3</em>
</div>
<br/>

<h3>
  Namizje prinaša pohitritve in izboljšave v uporabnosti
</h3>
<br>
<p align=justify>
  KDE-jevo namizno delovno okolje ponuja zmogljivo in kompletno doživetje pri delu z namizjem, ki prinaša odlično vklopljenost v operacijske sisteme GNU/Linux in UNIX. Ključni deli, ki sestavljajo KDE-jevo namizno delovno okolje so:
<ul>
  <li>
    <strong>KWin</strong>, zmogljiv upravljalnik oken, ki ponuja moderne 3D grafične učinke
  </li>
  <li>
    <strong>Namizno okolje Plasma</strong>, najnaprednejši sistem za namizje in pulte, ki z uporabo prilagodljivih gradnikov ponuja izboljšave produktivnosti in povezanost s spletom
  </li>
  <li>
    <strong>Dolphin</strong>, uporabniku prijazen upravljalnik datotek, ki se zaveda omrežja in vsebin
  </li>
  <li>
    <strong>KRunner</strong>, sistem za zaganjanje programov ter ukazov in iskanje uporabnih podatkov
  </li>
  <li>
    preprost dostop do nastavitev namizja in sistema s pomočjo <strong>Sistemskih nastavitev</strong>.
  </li>
</ul>
Spodaj najdete kratek seznam najpomembnejših izboljšav KDE-jevega namiznega delovnega okolja.
<ul>
  <li>
    <a href="http://plasma.kde.org">Namizno okolje Plasma</a> prihaja z <b>novo privzeto temo</b> Zrak. Zrak ima lahkotnejši videz in se bolje sklada s privzetim slogom za programe. Plasma je doživela tudi <b>precejšnje pohitritve</b>. Poraba pomnilnika se je zmanjšala, animacije pa so bolj tekoče. <b>Dejavnosti je sedaj možno povezati z navideznimi namizji</b>, kar vam omogoča, da imate na vsakem namizju druge gradnike. Nadalje je v Plasmi izboljšano <b>upravljanje z opravili in obvestili</b>. Tekoča opravila so združena v en pokazatelj napredka, s čimer je preprečeno odpiranje preveč pojavnih oken. Za prikaz obstoja tekočih opravil se uporabljajo animacije: okna tekoče zdrsnejo v sistemsko vrstico kjer je animirana ikona obvestil in opravil. Med manjše spremembe v Plasmi spadajo <b>povsem nastavljive tipkovne bližnjice</b> in bolj obširna navigacija s tipkovnico, zmožnost ustvarjanja gradnikov z vlečenjem ali kopiranjem vsebin na namizje, ter mnogo <b>novih in izboljšanih gradnikov</b>. Gradnik za prikaz mape vam sedaj omogoča <b>vpogled v mapo z lebdenjem nad njo</b>, <b>nov gradnik Prevajalnik</b> pa s pomočjo Google Prevajalnik prevaja besede in stavke kar na vašem namizju. KRunner omogoča <b>lažje odkrivanje zmožnosti</b>, saj je dodan gumb za pomoč, ki prikaže seznam ukazov in njihovo uporabo. <b>Dejanja lahko ponujajo manjše nastavitve</b>, kar na primer omogoča zaganjanje programov, kot da bi jih zagnal drug uporabnik.<br>
    <br>
  </li>
  <li>
    Upravljalnik datotek Dolphin <b>prikazuje majhne oglede datotek znotraj mape</b> in pomanjšane video posnetke, kar vam pomaga pri prepoznavanju predmetov. <b>Smeti lahko sedaj nastavite</b> iz Dolphinovega okna z nastavitvami. Različne omejitve velikosti smeti pomagajo pri preprečevanju, da bi zbrisane datoteke zapolnile disk. Priročni meni, ki se prikaže, če z desno kliknete na predmet, je sedaj nastavljiv; nastavitveno okno pa je bilo preoblikovano in je <b>lažje za uporabo</b>. Nova <b>lokacija network:/</b> prikazuje druge računalnike in storitve v vašem omrežju (trenutno omejeno na najavljeno po protokolu DNS-SD/Zeroconf, v prihodnjih različicah bo podpora razširjena).<br>
    <br>
  </li>
  <li>
    Nadaljnje izboljšave orodij delovnega okolja vam omogočajo lažje delo z računalnikom. <b>Sistemske nastavitve so hitrejše</b> in prinašajo <b>možnost drevesnega prikaza</b> nastavitev ter več izboljšav nastavitvenih modulov. <b>Novi učinki</b>, kot sta »List« in »Zdrs v ozadje«, in <b>pohitritve</b> za <b>KWin</b> napravijo upravljanje z okni bolj tekoče. Obenem integracija s temami za Plasmo napravi videz bolj skladen. <b>Klipper</b>, orodje, ki hrani zgodovino stvari skopiranih na odložišče, lahko sedaj <b>deluje ustrezno, glede na vsebino</b>. Samodejno določi seznam programov, ki lahko odprejo predmet na odložišču, in vam omogoča, da takoj zaženete želeni program.<br>
    <br>
  </li>
</ul>
</p>

<div class="text-center">
	<em>Um screencast apresentando algumas das melhorias mencionadas acima</em><br/>
<a href="http://blip.tv/file/get/Jospoortvliet-KDE43DesktopWorkspaceDemo820.ogv">Visoka kakovost v formatu Ogg Theora</a></div>
<br/>

<h3>
  Programi so napredovali
</h3>
<p align=justify>
  Skupnost KDE ponuja ogromno sofisticiranih programov, ki polno izkoriščajo zmogljivo KDE-jevo ogrodje za programe. Izbor teh programov je vključen v distribucijo KDE-jevega programja, ki je glede na kategorije razdeljena v različne pakete programov. Med te spadajo:
</p>
<ul>
  <li>
    KDE-jevi omrežni programi
  </li>
  <li>
    KDE-jeva večpredstavnost
  </li>
  <li>
    KDE-jeva grafična orodja
  </li>
  <li>
    KDE-jev paket PIM (za upravljanje z osebnimi podatki in komunikacijo)
  </li>
  <li>
    KDE-jevi izobraževalni programi
  </li>
  <li>
    KDE-jeve igre
  </li>
  <li>
    KDE-jeva orodja
  </li>
  <li>
    KDE-jevo okolje za razvoj programske opreme
  </li>
</ul>
<p align=justify>
  Skupaj tvorijo celovit nabor bistvenih programov za namizje, ki tečejo na večini modernih operacijskih sistemov. Spodaj boste našli izbor izboljšav nekaterih izmed programskih paketov.
</p>
<ul>
  <li>
    <strong>KDE-jeva orodja</strong> so doživela veliko izboljšav. V <b>KGpg</b>, orodje za šifriranje in podpisovanje datotek ter e-pošte je bil <b>integrirano Solid</b> za zaznavanje razpoložljivosti omrežne povezave. Prav tako ima izboljšano <b>pogovorno okno za uvoz ključev</b>. <b>Ark</b>, program za stiskanje in razširjanje datotek, sedaj <b>podpira LZMA/XZ</b>, ima izboljšano podporo za ZIP, RAR in 7zip in bolje podpira vleko in spuščanje. <b>KDELirc</b>, vmesnik za Linuxov sistem za infrardeče daljinske upravljalnike (LIRC), je bil prenesen na KDE 4 in je spet priložen. <b>Okteta</b>, KDE-jev urejevalnik binarnih datotek, je pridobil <b>orodje za nadzorne vsote</b>, stranski prikaz za brskanje po datotečnem sistemu in stranski prikaz zaznamkov. <strong>Lokalize</strong>, KDE-jevo orodje za prevajanje, prinaša podporo za skripte, nove datotečne formate in <b>prevajanje dokumentov OpenDocument (ODF)</b>.<br>
    <br>
  </li>
  <li>
    <b>KDE-jeve igre</b> sedaj po večini uporabljajo podobne <b>teme v slogu starega Egipta</b>. V KGoldRunner je nova igra <b>»Prekletstvo mumije«</b>, izboljšano pa je bilo tudi samo igranje, z natančnejšim premorom, nadaljevanjem, snemanjem in ponovnim igranjem iger. KMahjongg vsebuje 70 novih stopenj, ki so jih prispevali uporabniki. Dodana je bila <b>nova igra KTron</b>. Nekatere igre so dobile nove zmožnosti, kot sta na primer Uparjevalnik v igri Killbots in boljši računalniški igralec v Bovo. Zahvaljujoč delu na nalaganju in shranjevanju datotek stanja raztegljive grafike se mnoge igre <b>zaženejo in tečejo hitreje</b>.<br>
    <br>
  </li>
  <li>
    <strong>KDE-jevi programi za upravljanje z osebnimi podatki</strong> so doživeli izboljšave na več področjih, na primer pri hitrosti in stabilnosti. Takojšnji sporočilnik <b>Kopete</b> ima izboljšan seznam stikov, KOrganizer pa se lahko usklajuje z <b>Google Koledar</b>. KMail podpira vstavljanje slik v e-pošto, <b>KAlarm</b> pa je pridobil zmožnost izvažanja, podporo za vlečenje in spuščanje in izboljšano nastavljanje.<br>
    <br>
  </li>
</ul>

<div class="text-center">
	<a href="/announcements/4/4.3.0/games.png">
	<img src="/announcements/4/4.3.0/games_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nekatere egipčanske teme v igrah</em>
</div>
<br/>

<ul>
  <li>
    Če gre s KDE-jevim programom kaj narobe in se sesuje, vam bo <b>novo orodje za poročanje o napakah</b> olajšalo prispevati k izboljšanju stabilnosti KDE-ja. Orodje za poročanje o napakah prinaša ocenjevanje kakovosti podatkov o sesutju z največ tremi zvezdicami. Ponudi tudi nasvete za izboljšanje kakovosti podatkov o sesutju in samega poročila o napaki, ter vas vodi skozi poročanje. V obdobju beta različic se je novo orodje že izkazalo z dvigom kakovosti poročil o napakah.<br>
  </li>
</ul>
</p>
<div class="ii gt" id=:7r>
</div>
<h3>
  Okolje pospešuje razvoj
</h3>
<p align=justify>
  Skupnost KDE s KDE-jevim programskim ogrodjem (KDE Application Framework) v ospredje prinaša mnoge inovacije za razvijalce programov. To integrirano in konsistenčno ogrodje, ki gradi na prednostih knjižnice Qt podjetja Nokia, je bilo ustvarjeno kot neposreden odgovor na dejanske potrebe razvijalcev programske opreme.
</p>
<p align=justify>
  KDE-jevo programsko ogrodje razvijalcem pomaga ustvariti robustne programe na učinkovit način, tako da poenostavi kompleksnost in utrujajoča opravila, ki sta običajno povezana z razvojem programske opreme. KDE-jevi programi z uporabo ogrodja ponujajo prepričljiv prikaz prilagodljivosti in uporabnosti KDE-jevega programskega ogrodja.
</p>
<p align=justify>
  Izdano pod licenco LGPL (ki omogoča razvoj prostih/odprto-kodnih in tudi lastniških/zaprto-kodnih programov), na voljo za več okolij (GNU/Linux, BSD, UNIX, Mac OS in Windows) med drugim vsebuje zmogljiv komponentni model (<b>KParts</b>), omrežno-transparentni dostop do podatkov (<b>KIO</b>) in prilagodljivo upravljanje z nastavitvami. Na voljo je na ducate uporabnih gradnikov, od pogovornih oken za datoteke do izbirnikov pisav, ogrodje pa ponuja tudi integracijo za semantično iskanje in druge semantične storitve (<b>Nepomuk</b>), zavedanje stanja strojne opreme (<b>Solid</b>) in predvajanje večpredstavnih vsebin (<b>Phonon</b>). V nadaljevanju si preberite o izboljšavah KDE-jevega programskega ogrodja.
</p>
<ul>
  <li>
    KDE-jevo programsko ogrodje v KDE 4.3 prvič ponuja začetke integracije storitve <b><a href="http://www.socialdesktop.org/">Social Desktop</a></b>, ki prinaša svetovno skupnost uporabnikov prostega programja neposredno na vaše namizje. Iniciativa Social Desktop ponuja <strong>odprto platformo za sodelovanje, izmenjevanje in komunikacijo</strong>. Njen namen je ljudem omogočiti izmenjavo znanja, brez da bi kontrolo predali zunanji organizaciji. Platforma trenutno ponuja <strong>podatkovni pogon</strong> za gradnike za Plasmo, ki podpirajo dele storitve Social Desktop.<br>
    <br>
  </li>
  <li>
    <strong>Novi protokol za sistemsko vrstico</strong>, ki je bil razvit v sodelovanju z iniciativo <a href="http://www.freedesktop.org/wiki/">Free Desktop</a>, je že dolgo potrebna prenova stare specifikacije za sistemsko vrstico. Stara sistemska vrstica, ki je uporabljala majhna vgrajena okna, ni imela nobene možnosti nadzora nad svojo vsebino, kar je omejevalo prilagodljovost tako uporabnikom kot tudi za razvijalcem programov. Nova sistemska vrstica sicer podpira tako stari kot tudi novi standard, vendar je od razvijalcev programov sedaj pričakovano, da posodobijo svoje programe in uporabljajo novo specifikacijo. Za dodatne podatke si <a href="http://www.notmart.org/index/Software/Systray_finally_in_action">preberite ta blog</a> ali pa <a href="http://techbase.kde.org/Projects/Plasma/NewSystemTray">stran na TechBase</a>.<br>
    <br>
  </li>
  <li>
    Namizno okolje Plasma prinaša <strong>podatkovni pogon za geolokacijo</strong>, ki uporablja knjižnico libgps in HostIP. Podatkovni pogon gradnikom omogoča odzivanje glede na trenutno lokacijo uporabnika. Poleg raznih izboljšav obstoječih podatkovnih pogonov <strong>novi podatkovni pogoni</strong> omogočajo dostop do <strong>virov Akonadija</strong> (kar vključuje e-pošto in koledar), dostop do <a href="http://nepomuk.semanticdesktop.org/">semantičnih metapodatkov Nepomuka</a> in stanja tipkovnice. O odkrivanju in uporabi podatkovnih pogonov si <a href="http://techbase.kde.org/Development/Tutorials/Plasma/DataEngines">več preberite na TechBase</a>.<br>
    <br>
  </li>
  <li>
    KDE-jeva programska platforma po novem prinaša <strong>vmesnik za PolicyKit</strong>, kar razvijalcem poenostavi delo, pri njihovem programu, ki mora izvajati privilegirana dejanja na varen, konsistenčen in preprost način. Priloženi so upravljalnik dovoljenj, posrednik za overjanje in knjižnica za razvijalce, ki so preproste za uporabo. Preberite si <a href="http://techbase.kde.org/Development/Tutorials/PolicyKit/Introduction">vodnika na TechBase</a>.<br>
    <br>
  </li>
  <li>
    <strong>Akonadi</strong>, rešitev za hranjenje osebnih podatkov pobude Free Desktop je <strong>pripravljena za široko uporabo</strong>. Poleg razpoložljivosti podatkovnega pogona za Plasmo razvijalcem programov priporočamo, da si ogledajo <a href="http://techbase.kde.org/Projects/PIM/Akonadi">stran o Akonadi na TechBase</a>, posebno če njihov program potrebuje (bralni in/ali pisalni) dostop do zgodovine klepetov, e-pošte, blogov, kontaktov ali drugih osebnih podatkov. Akonadi je tehnologija za več platform/namizij in ponuja dostop do kakršne koli vrste podatkov ter je zasnovana za obvladovanje velikih količin podatkov, kar omogoča uporabo v velikem številu primerov.<br>
    <br>
  </li>
</ul>
</p>

<div class="text-center">
	<a href="/announcements/4/4.3.0/social.png">
	<img src="/announcements/4/4.3.0/social_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Socialno namizje in druge spletne storitve</em>
</div>
<br/>

<h4>
  Razširite novico
</h4>
<p align="justify">
  Skupnost KDE vsakogar spodbuja, da <strong>razširi novico</strong> prek socialnih omrežij. Pošljite novico na strani kot so Delicious, Digg, Reddit ali prek mikrobloganja na Twitter in identi.ca. Zaslonske posnetke objavite na Facebook, Flickr, ipernity in Picasa. Pri tem izberite ustrezno skupino. Posnemite video in ga objavite na Dailymotion, YouTube, Blip.tv, Vimeo ali drugo podobno stran. Vse kar objavite označite vsaj <em>z oznako <strong>kde43</strong></em>, da bodo vsi lahko lažje našli vse te prispevke. Skupnost KDE bo na ta način tudi lažje sestavila poročilo o odzivu na izid KDE 4.3. <strong>Pomagajte nam razširiti novico in postanite del nje!</strong>
</p>

<p align="justify">
  Vse kar se na socialnem spletu dogaja okoli izida KDE 4.3 lahko v živo spremljate na povsem novi strani <a href="http://buzz.kde.org/"><strong>KDE Community livefeed</strong></a>. na tej strani se v živo zbirajo odzivi z identi.ca, Twitter, YouTube, Flickr, Picasa, blogov in mnogih drugih spletnih strani za druženje in mreženje. Obiščite <strong><a href="http://buzz.kde.org/">buzz.kde.org</a></strong> in bodite na tekočem. Namig: na namizje lahko dodate gradnik Spletni brskalnik in v njem odprete to stran.
</p>

<center>
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_KDE_4_3_0_Caizen_Release_Announcement"><img src="/announcements/buttons/digg.gif" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/97gdu/kde_430_caizen_released/"><img src="/announcements/buttons/reddit.gif" /></a>
    </td>
    <td>
        <a href="http://www.twitter.com"><img src="/announcements/buttons/twitter.gif" /></a>
    </td>
    <td>
        <a href="http://www.identi.ca"><img src="/announcements/buttons/identica.gif" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde43/"><img src="/announcements/buttons/flickr.gif" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde43"><img src="/announcements/buttons/youtube.gif" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com"><img src="/announcements/buttons/facebook.gif" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde43"><img src="/announcements/buttons/delicious.gif" /></a>
    </td>
</tr>
</table>
<style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">microgumbi</a></style>
</center>

<h4>
  Namestitev KDE 4.3.0
</h4>
<p align="justify">
  KDE, vključno z vsemi knjižnicami in programi, je na prosto voljo pod odprto-kodnimi licencami. KDE lahko v obliki izvorne kode ali binarni obliki prenesete z <a href="http://download.kde.org/stable/4.3.0/">download.kde.org</a>, ga dobite <a href="/download">na zgoščenki</a>, ali pa je na voljo kot <a href="/distributions">del večjih distribucij operacijskih sistemov GNU/Linux, BSD in UNIX</a>.
</p>
<p align="justify">
  <em>Binarni paketi</em>:
  Nekateri ponudniki Linuxa ali UNIXa so pripravili binarne pakete s KDE 4.3.0 za nekatere različice svojih distribucij. V drugih primerih so to storili prostovoljci iz skupnosti. Ti binarni paketi so morda na voljo za prost prenos s KDE-jeve strani <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.3.0/">download.kde.org</a>. Dodatni binarni paketi in posodobitve trenutnih paketov bodo na voljo v prihodnjih tednih.
</p>

<p align="justify">
  <a id="package_locations"><em>Lokacije paketov</em></a>:
  Za trenutni seznam razpoložljivih binarnih paketov, o katerih je bila obveščena skupnost KDE, obiščite stran <a href="/info/4/4.3.0">KDE 4.3.0 Info</a>.
</p>

<p align="justify">
  Težave s hitrostjo zaprto-kodnih grafičnih gonilnikov podjetja NVidia so bile <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">odpravljene</a> v najnovejši različici gonilnikov.
</p>

<h4>
  Prevajanje izvorne kode KDE 4.3.0
</h4>
<p align="justify">
  <a id="source_code"></a>
  Celotna izvorna koda za KDE 4.3.0 je na voljo za <a href="http://download.kde.org/stable/4.3.0/src/">prost prenos</a>. Navodila za prevajanje in namestitev KDE 4.3.0 so na voljo na strani <a href="/info/4/4.3.0#binary">KDE 4.3.0 Info</a>.
</p>



