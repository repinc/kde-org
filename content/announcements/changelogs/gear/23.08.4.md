---
aliases:
- ../../fulllog_releases-23.08.4
title: KDE Gear 23.08.4 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Adapt to renamed activities library repo. [Commit](http://commits.kde.org/dolphin/475da39174a19559a9b19bbb9c4183ef918d1caf).
{{< /details >}}
{{< details id="dolphin-plugins" title="dolphin-plugins" link="https://commits.kde.org/dolphin-plugins" >}}
+ Mountisoaction: add preferred raw disk mimetypes. [Commit](http://commits.kde.org/dolphin-plugins/507052906beca5d66991d156036bec0d75708556). Fixes bug [#475659](https://bugs.kde.org/475659).
{{< /details >}}
{{< details id="falkon" title="falkon" link="https://commits.kde.org/falkon" >}}
+ Process the "--help-all" CLI argument. [Commit](http://commits.kde.org/falkon/b2d2b7268f8999b7f6c6353674098f248d2c9a39). Fixes bug [#477492](https://bugs.kde.org/477492).
+ Fix StartPage search engine default configuration. [Commit](http://commits.kde.org/falkon/00a1b8c1009b638aca1a0d41876a317186420741). Fixes bug [#419530](https://bugs.kde.org/419530).
+ Fix crash when starting new session after crash. [Commit](http://commits.kde.org/falkon/709c047ac891f6d43077e5e5c41b3c1c86285130). Fixes bug [#408701](https://bugs.kde.org/408701).
{{< /details >}}
{{< details id="granatier" title="granatier" link="https://commits.kde.org/granatier" >}}
+ Player selection dialog view: render player icons hidpi-aware. [Commit](http://commits.kde.org/granatier/8afcc09b5602ede2b2efbcaca624cad09cafc62f).
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Adapt to renamed activities library repo. [Commit](http://commits.kde.org/gwenview/34e9c04dc4a80f34e28945074f9cc8978e9451ce).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Set bug address in KAboutData for the about dialog bug reporting action. [Commit](http://commits.kde.org/itinerary/f702f2d46b2aae303eb2da983862c3d4ec309a4f).
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Show correct calendar names when deleting. [Commit](http://commits.kde.org/kalarm/940eb384c86b040b78c5b269ad47e1f28a961845).
{{< /details >}}
{{< details id="kalgebra" title="kalgebra" link="https://commits.kde.org/kalgebra" >}}
+ Adapt to plasma-framework being renamed to libplasma. [Commit](http://commits.kde.org/kalgebra/a5a1643b2e8908501f91ec858f105496cdce49f5).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Adapt to renamed activities library repo. [Commit](http://commits.kde.org/kate/cc0c3284cfd2dacd87c07b6d09e75ea7fd344ac4).
{{< /details >}}
{{< details id="katomic" title="katomic" link="https://commits.kde.org/katomic" >}}
+ Render molecule preview hidpi-aware. [Commit](http://commits.kde.org/katomic/20e332f5eda445a2c854813b977362cf823d5c37).
{{< /details >}}
{{< details id="kclock" title="kclock" link="https://commits.kde.org/kclock" >}}
+ Adapt to plasma-framework being renamed to libplasma. [Commit](http://commits.kde.org/kclock/c3226d5702f9119d108af74b1e014e665d086ea5).
+ Adjust to plasma-framework moving to Plasma. [Commit](http://commits.kde.org/kclock/062d5b144e0e140fd62df9a09287bd36596f87b4).
{{< /details >}}
{{< details id="kdenetwork-filesharing" title="kdenetwork-filesharing" link="https://commits.kde.org/kdenetwork-filesharing" >}}
+ Packagekit: allow interactive auth dialogs. [Commit](http://commits.kde.org/kdenetwork-filesharing/6cbdd2bd71b3d6f030f0611497874eeee3414d4f). Fixes bug [#472145](https://bugs.kde.org/472145).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix transparency lost on rendering nested sequences. [Commit](http://commits.kde.org/kdenlive/a9a6067bdf41024c4fb8590279895f5ce5e74adf). Fixes bug [#477771](https://bugs.kde.org/477771).
+ Fix guides categories not applied on new document. [Commit](http://commits.kde.org/kdenlive/127584384c1112638952e51025e3551b528779fb). Fixes bug [#477617](https://bugs.kde.org/477617).
+ Check MLT's render profiles for missing codecs. [Commit](http://commits.kde.org/kdenlive/f7467c6433b9f5f97d274d0bb7c46c00c5b1a465). See bug [#475029](https://bugs.kde.org/475029).
+ Fix crash on auto subtitle with subtitle track selected. [Commit](http://commits.kde.org/kdenlive/bd0645e73799cd10430de923e7946b8698568add).
+ Fix qml warning (incorrect number of args). [Commit](http://commits.kde.org/kdenlive/360b30ac346ebc7df364402872976281d7c3dd44).
+ Fix audio stem export. [Commit](http://commits.kde.org/kdenlive/90785e76e240b231544936754fb4f4c6c8b2fa59).
+ When pasting clips to another project, disable proxies. [Commit](http://commits.kde.org/kdenlive/d6ac6eb96cf75c271639f8a4f0f48a51bb25d894).
+ Don't allow creating profile with non standard and non integer fps from a clip. [Commit](http://commits.kde.org/kdenlive/e6abc4331e053bfdfe9e61ac1eb4b7cb2fc27344).
+ Fix mix not always deleted when moving grouped clips on same track. [Commit](http://commits.kde.org/kdenlive/9202b7593e8d8fec908107b7ecd84d26568d41cf).
+ Fix remap crashes. [Commit](http://commits.kde.org/kdenlive/3acd3166f73860241ead56f9885b2d2c45124346).
+ Ensure timeremap option is disabled when effect is deleted. [Commit](http://commits.kde.org/kdenlive/c577bf2df813929f8bf9560838bdcdf12963dd40).
+ Time remap: fix changing speed broken / crashing. [Commit](http://commits.kde.org/kdenlive/f9e816fb502513254c893e9b1c01b2cd442a7001).
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Add default ews base url. [Commit](http://commits.kde.org/kdepim-runtime/229b79e35c2e45cea4e501d739e942d40ba16e63).
+ Correctly reload configuration. [Commit](http://commits.kde.org/kdepim-runtime/fdaec5c3f9ac54fa1978a8f31230c5c2325cb931). Fixes bug [#473897](https://bugs.kde.org/473897).
+ Don't destroy save password job before closing the save dialog. [Commit](http://commits.kde.org/kdepim-runtime/ed0038cba13f0702efae24a91dc2a77cb68d5908).
+ Do not read google password from wallet when account is not configured. [Commit](http://commits.kde.org/kdepim-runtime/7bff8188dcfeedaae54e1e7effd97ea6bff21f2d).
{{< /details >}}
{{< details id="kgeography" title="kgeography" link="https://commits.kde.org/kgeography" >}}
+ Update flag of Myanmar. [Commit](http://commits.kde.org/kgeography/7c27d2bc66da40517c2a6081660fccf27689b106). Fixes bug [#477172](https://bugs.kde.org/477172).
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Adapt to renamed activities library repo. [Commit](http://commits.kde.org/kio-extras/7fac8f9780a2054a17fe2daf67ed10a763a27d37).
+ Fix build with MinGW. [Commit](http://commits.kde.org/kio-extras/0fcbe181bef6f6ed36dea664918014b6caed9955). Fixes bug [#430312](https://bugs.kde.org/430312).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Fix Motel One checkin time when running in a different timezone. [Commit](http://commits.kde.org/kitinerary/6921a467df3b50b943659b48bfaba81cb4d58b9d).
+ Add Motel One pkpass extractor script. [Commit](http://commits.kde.org/kitinerary/fcb20729dc6ac36bcb05d86790815f5494a6cfc5).
+ Add stand-alone filter condition for SAS itinerary PDFs. [Commit](http://commits.kde.org/kitinerary/19127bfa172a7a5863c2b3b023af5e3e2f691c77).
+ Handle another Flixbus Unicode station icon variant. [Commit](http://commits.kde.org/kitinerary/d5a5b1092c9b4c5c0346950b177dd7c5a6cfaa46).
+ Ignore ERA FCB UIC station codes in Germany. [Commit](http://commits.kde.org/kitinerary/d29453c3838b26bdd7cadcc1ce9af7fed4e71b5c).
+ Use the entire UIC 918.3 station object, not just the identifier. [Commit](http://commits.kde.org/kitinerary/2823cecb8518df29ce12317598d628d2d21ccf57).
+ Fix ERA FCB arrival time parsing. [Commit](http://commits.kde.org/kitinerary/1bf85c09bd14ba7bf4cad4d04244f44cda65e523).
+ Support Dutch-language Eurostar (Thalys) tickets. [Commit](http://commits.kde.org/kitinerary/f37f43ae2143c5cc45927a3c568326a02ba1766f).
+ Increase the maximum document size thresholds. [Commit](http://commits.kde.org/kitinerary/dc34c15d8067e842018e30e96456bfa4896ffb1c).
+ Add basic support for 12go.asia PDFs. [Commit](http://commits.kde.org/kitinerary/359eaebf4d433a8f843fdb0cb648703a02320cda).
{{< /details >}}
{{< details id="kmines" title="kmines" link="https://commits.kde.org/kmines" >}}
+ Fix initial logging errors from KGameRenderer due to too large pixmaps. [Commit](http://commits.kde.org/kmines/376c17a28314095aed4d2691dd288707e3e3957e).
{{< /details >}}
{{< details id="knights" title="knights" link="https://commits.kde.org/knights" >}}
+ Set the difficulty level correctly. [Commit](http://commits.kde.org/knights/7f2a9e7e733e1de6bed7fb87484238484d4e5688). Fixes bug [#472817](https://bugs.kde.org/472817).
+ Adapt to plasma-framework being renamed to libplasma. [Commit](http://commits.kde.org/knights/d84f69735e180c18c7059e253f844bd66ddb702d).
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Adapt to renamed activities library repo. [Commit](http://commits.kde.org/konqueror/a7c2b25ec698519a1610830fd2ec0a9272a2230d).
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Apply split view icons to the session context menu too. [Commit](http://commits.kde.org/konsole/f6da91ea06e631e94f24149b8d14039fa287a3a6).
{{< /details >}}
{{< details id="kontrast" title="kontrast" link="https://commits.kde.org/kontrast" >}}
+ Include QCoreApplication for qApp. [Commit](http://commits.kde.org/kontrast/cddd118a2a7d716699814122f67ebb0b59814921).
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Fix infinite loop in mEventViewer->setIncidence due to invalid date. [Commit](http://commits.kde.org/korganizer/82287638ae06b63dafdd73b01d1c41847467e88e).
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Add Eurostar onboard SSID. [Commit](http://commits.kde.org/kpublictransport/cf4fc69a82a557d52af4871d550e7e574d1618c7).
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ Adapt to renamed activities library repo. [Commit](http://commits.kde.org/krdc/9abc21e2edc6f30ffbc250a02219aacc82ce2135).
{{< /details >}}
{{< details id="krfb" title="krfb" link="https://commits.kde.org/krfb" >}}
+ Adjust to KWayland moving to Plasma. [Commit](http://commits.kde.org/krfb/9b90e28d9df56887814fc254664db0e23cd78011).
{{< /details >}}
{{< details id="ktouch" title="ktouch" link="https://commits.kde.org/ktouch" >}}
+ "Pisteen jälkeen on aina välilyönti." -> added space after ".". [Commit](http://commits.kde.org/ktouch/8b1387248f6f57fca4e81f73b533a357aa9cf9f4).
{{< /details >}}
{{< details id="kweather" title="kweather" link="https://commits.kde.org/kweather" >}}
+ Adapt to plasma-framework being renamed to libplasma. [Commit](http://commits.kde.org/kweather/58fd443acaef2da35dd477309e4c30c8052a10b5).
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ KGameRenderer: handle requests for images with too large or invalid sizes. [Commit](http://commits.kde.org/libkdegames/6dbe307305d1a2c3cd963dc66b7c9f6c312ff693).
{{< /details >}}
{{< details id="merkuro" title="merkuro" link="https://commits.kde.org/merkuro" >}}
+ Fix wrong bug url. [Commit](http://commits.kde.org/merkuro/7d2dafbfc0fde3c59e79a53f9bc3c47faa1c9e9a). Fixes bug [#477373](https://bugs.kde.org/477373).
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ Fix crash for forbidden non-mxc media. [Commit](http://commits.kde.org/neochat/7e8a8615b13c41c9003f1d9ad9f227ba8f2fd207). Fixes bug [#476698](https://bugs.kde.org/476698).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Adapt to renamed activities library repo. [Commit](http://commits.kde.org/okular/14a35b9c47e78d3d9aaa19a0739148a86090a295).
{{< /details >}}
{{< details id="plasmatube" title="plasmatube" link="https://commits.kde.org/plasmatube" >}}
+ Change audio client name to PlasmaTube. [Commit](http://commits.kde.org/plasmatube/af3b5fb69250189fd252a2fe1e83819744a866dc). Fixes bug [#475968](https://bugs.kde.org/475968).
+ Stop drawer from showing itself when fullscreen. [Commit](http://commits.kde.org/plasmatube/26d2585c5ba7531386f30cfce932b45f57423bb2). Fixes bug [#476007](https://bugs.kde.org/476007).
{{< /details >}}
{{< details id="print-manager" title="print-manager" link="https://commits.kde.org/print-manager" >}}
+ Adapt to plasma-framework being renamed to libplasma. [Commit](http://commits.kde.org/print-manager/c5a04cc01ed4e697b4fd4d7357e65118da6eb03e).
+ Adjust to plasma-framework moving to Plasma. [Commit](http://commits.kde.org/print-manager/f3c5717550cd95fc404d49497be79e5ca38e0e17).
{{< /details >}}
{{< details id="sweeper" title="sweeper" link="https://commits.kde.org/sweeper" >}}
+ Adapt to plasma-framework being renamed to libplasma. [Commit](http://commits.kde.org/sweeper/142a60bbaa210ac1b5e7ff203da25b197f177f7c).
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Force the fullscreen image viewer focus so keybinds work. [Commit](http://commits.kde.org/tokodon/7f09167f8b5fd76e2555cf2971706cee3b338b1a).
+ Protect against a notification missing a post. [Commit](http://commits.kde.org/tokodon/c726d53e5c698429d8e1e4a8669d6a053abb6d62). Fixes bug [#475825](https://bugs.kde.org/475825).
{{< /details >}}
{{< details id="yakuake" title="yakuake" link="https://commits.kde.org/yakuake" >}}
+ Adjust to KWayland moving to Plasma. [Commit](http://commits.kde.org/yakuake/67bbfce05b9fbdf166b76fd11a4db42ebd5809df).
{{< /details >}}
