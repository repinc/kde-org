---
title: Plasma 5.23.3 complete changelog
version: 5.23.3
hidden: true
plasma: true
type: fulllog
---
{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Kstyle: Add QFocusFrame to non-view/delegate interactive widget. [Commit.](http://commits.kde.org/breeze/6515aa667d797453da8476c1291a618349af2fdd) Fixes bug [#443469](https://bugs.kde.org/443469)
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Correctly apply initial xdg-shell double buffered state. [Commit.](http://commits.kde.org/kwayland-server/5e9394953f2a4e8f6c086e52fa3149244f77fc42) 
+ Fix ownership of SlideInterface. [Commit.](http://commits.kde.org/kwayland-server/3a2870e73978207e685b3b47bb4698cf6e0af750) 
+ Emit DataOfferInterface::dragAndDropActionsChanged() only if dnd actions change. [Commit.](http://commits.kde.org/kwayland-server/6fec0b86abf3038a5719c5d0977dbf60c156966a) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Wayland: Check workspace position when preferred deco mode changes. [Commit.](http://commits.kde.org/kwin/e2f9f35c3cb07503e5ffd7b0134526ec0edddc83) 
+ DecorationItem: Prevent crash. [Commit.](http://commits.kde.org/kwin/15a30e75869f7a55d231f71712ad4c504591376a) 
+ Fixup! [wayland] fix ignored keyboard RepeatRate. [Commit.](http://commits.kde.org/kwin/ac081cad79930a419d413858ca2c3400d571d569) Fixes bug [#443721](https://bugs.kde.org/443721)
+ RenderLoop: restrict repaint scheduling with fullscreen windows. [Commit.](http://commits.kde.org/kwin/638f5482a854d4538fa3e58588d8d33d9e938618) Fixes bug [#443872](https://bugs.kde.org/443872)
+ Wayland: Fix wayland windows growing after toggling decorations. [Commit.](http://commits.kde.org/kwin/ceec4e50508fa92f55ad8581586831fa2d8fe605) Fixes bug [#444119](https://bugs.kde.org/444119)
+ Use QScopedPointer to store decoration object. [Commit.](http://commits.kde.org/kwin/71a5b00d9972f843305f63949f510cf6d285dce7) 
+ Scripts/minimizeall: Try to preserve last active window. [Commit.](http://commits.kde.org/kwin/ea84b1caadf6d061af4b2efd9e9b953477ff444e) 
+ Screencasting: Do not crash when the platform cannot provide textures. [Commit.](http://commits.kde.org/kwin/e4b279866e26f4388d6a3dc7e7b4c1ac4efd72ac) Fixes bug [#442711](https://bugs.kde.org/442711)
+ Effects/slidingpopups: Reload slide data after restarting compositing. [Commit.](http://commits.kde.org/kwin/f0cce272adb78ade2a338e5949c93bb5b7b11827) See bug [#443711](https://bugs.kde.org/443711)
+ Effects/slidingpopups: Schedule workspace repaints. [Commit.](http://commits.kde.org/kwin/8ca83ef190dfb0bf32719073fc7360ddfea799b6) Fixes bug [#444502](https://bugs.kde.org/444502)
+ Platforms/drm: only use glDrawBuffer with desktop GL. [Commit.](http://commits.kde.org/kwin/c4c03e2559f1ecf6f80ae965230bf0a19e17e210) 
+ [keyboard kcm] fix lost Locks state on keymap reconfigure. [Commit.](http://commits.kde.org/kwin/451bc8dc248d9eef14af6834d3dfd52040472675) Fixes bug [#443739](https://bugs.kde.org/443739)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Fix ksgrd_network_helper crash on exit afer a failure. [Commit.](http://commits.kde.org/libksysguard/1344cf98af560570fc661b175643b7a90cb7ac52) Fixes bug [#444921](https://bugs.kde.org/444921)
+ Fix incorrect string termination in ConnectionMapping. [Commit.](http://commits.kde.org/libksysguard/203b72c209a9642fbf260e37a641a0ec8469797a) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Keyboardlayout: Fix missing Esperanto flag icon in the menu. [Commit.](http://commits.kde.org/plasma-desktop/50a42b490ef595c3872c2c60b57cf7240c2a7b46) Fixes bug [#445077](https://bugs.kde.org/445077)
+ Kickoff: Do not accept stylus in TapHandler. [Commit.](http://commits.kde.org/plasma-desktop/9f9dc34cadf839871a0fe6fc7e3b412f9f2b028d) Fixes bug [#445111](https://bugs.kde.org/445111)
+ DRAFT: Fix issues with LayoutManager.insertAtCoordinates. [Commit.](http://commits.kde.org/plasma-desktop/a47ead6339eb516a2d84a102088eb380ae118c8d) Fixes bug [#444071](https://bugs.kde.org/444071)
+ Revert "taskmanager: Show highlight before ToolTipInstance starts loading for grouped tooltips". [Commit.](http://commits.kde.org/plasma-desktop/9c82d1f57b962d6e71755100c72f10335db6e9a5) Fixes bug [#444633](https://bugs.kde.org/444633)
+ Do not unnecessarily reset panel opacity when starting an animation. [Commit.](http://commits.kde.org/plasma-desktop/7cdd39f12bff708027d35247f8dc9a2c5d8bd556) Fixes bug [#437093](https://bugs.kde.org/437093)
+ Foldermodel: Drop overlay(s) to icons in plasma folder view. [Commit.](http://commits.kde.org/plasma-desktop/7ccbdb322e0ba6ec47788da05009decace77999a) Fixes bug [#444514](https://bugs.kde.org/444514)
+ Plasma-desktop runner: Fix showing interactive plasma/kwin console. [Commit.](http://commits.kde.org/plasma-desktop/e880214da9c1cd7536cbce82395b858811aff2c8) Fixes bug [#444366](https://bugs.kde.org/444366)
+ Kickoff: Fix drag and drop causing delegates to reset to a 0 X position and overlap. [Commit.](http://commits.kde.org/plasma-desktop/af9aeeccfa2a6efb16e4c5904768c55f97c022ff) Fixes bug [#443975](https://bugs.kde.org/443975)
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ OpenVPN: fix logic in auth dialog. [Commit.](http://commits.kde.org/plasma-nm/48fad4ac77520d673414ef957e6dedc4d151eb73) Fixes bug [#444882](https://bugs.kde.org/444882)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Fixed size hints to the osd. [Commit.](http://commits.kde.org/plasma-workspace/f170b5f729c6f13c9864440c01cad0b53444ba9a) 
+ Use implicit size to not cause binding loops. [Commit.](http://commits.kde.org/plasma-workspace/2d3e2b7da8d26a8e52ea7f0980314a6123603fc5) Fixes bug [#422072](https://bugs.kde.org/422072)
+ Wayland: don't create DesktopView for placeholder screen. [Commit.](http://commits.kde.org/plasma-workspace/7dd3aa10d0e62ff76ed965f0069b9035bf7565b3) Fixes bug [#438839](https://bugs.kde.org/438839). Fixes bug [#444801](https://bugs.kde.org/444801)
+ [startplasma] Detect systemd service in linked state. [Commit.](http://commits.kde.org/plasma-workspace/b46781055caa1e519e56533f4d306fd1fcc66b66) 
+ [Notifications] Fix grouping container side line. [Commit.](http://commits.kde.org/plasma-workspace/f6f972863c7ef46e1b1848ac431571ba81bf455b) 
+ Disconnect watcher for xdgActivationTokenArrived. [Commit.](http://commits.kde.org/plasma-workspace/7f18bc04d74117525141d43f857f8d9f03b17ce3) Fixes bug [#444385](https://bugs.kde.org/444385)
+ Interactiveconsole: Allow specifying mode from command line parameter. [Commit.](http://commits.kde.org/plasma-workspace/46eb52dcd8a525b0be3b4a52fdda0dfa19da5dfc) See bug [#444366](https://bugs.kde.org/444366)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ On FreeBSD do not try to execute powerdevil.backlighthelper.syspath action. [Commit.](http://commits.kde.org/powerdevil/747f684fcb65206d23091b3134dc9256598298e3) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Use handlers in sub category header. [Commit.](http://commits.kde.org/systemsettings/f2a220c1f0f7d0ae28b1b644eef5f60791c0c8d0) 
{{< /details >}}

