2008-06-18 22:00 +0000 [r822031-822029]  woebbe

	* branches/KDE/3.5/kdesdk/cervisia/updateview.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/updateview_items.cpp,
	  branches/KDE/3.5/kdesdk/cervisia/updateview.h,
	  branches/KDE/3.5/kdesdk/cervisia/ChangeLog: Backport of rev.
	  821988 and 822002: Fixed crash when updating the status after
	  adding a directory to a repository. In this case we had a
	  dangling pointer which caused the crash.

	* branches/KDE/3.5/kdesdk/cervisia/Makefile.am,
	  branches/KDE/3.5/kdesdk/cervisia/version.h: bump version

2008-06-29 22:53 +0000 [r826183]  anagl

	* branches/KDE/3.5/kdesdk/kbabel/kbabeldict/modules/poauxiliary/poauxiliary.desktop,
	  branches/KDE/3.5/kdesdk/kioslave/svn/svnhelper/subversion.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/kbabeldict/kbabeldict.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/not-translated/kbabel_nottranslatedtool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/punctuation/kbabel_punctuationtool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/catalogmanager/catalogmanager.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/regexp/kbabel_regexptool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/kbabeldict/kbabeldict_module.desktop,
	  branches/KDE/3.5/kdesdk/kioslave/svn/ksvnd/ksvnd.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/common/kbabelfilter.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/whitespace/kbabel_whitespacetool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/filters/xliff/kbabel_xliff_import.desktop,
	  branches/KDE/3.5/kdesdk/kioslave/svn/svnhelper/subversion_toplevel.desktop,
	  branches/KDE/3.5/kdesdk/kompare/komparenavigationpart.desktop,
	  branches/KDE/3.5/kdesdk/kfile-plugins/diff/kfile_diff.desktop,
	  branches/KDE/3.5/kdesdk/kcachegrind/kcachegrind/kcachegrind.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/xml/kbabel_xmltool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/arguments/kbabel_argstool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/filters/xliff/kbabel_xliff_export.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/accelerators/kbabel_accelstool.desktop,
	  branches/KDE/3.5/kdesdk/kuiviewer/kuiviewer_part.desktop,
	  branches/KDE/3.5/kdesdk/cervisia/cervisia.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/kbabeldict/modules/pocompendium/pocompendium.desktop,
	  branches/KDE/3.5/kdesdk/kcachegrind/kcachegrind/x-kcachegrind.desktop,
	  branches/KDE/3.5/kdesdk/kuiviewer/designerthumbnail.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/setfuzzy/kbabel_setfuzzytool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/kbabeldict/modules/dbsearchengine/dbsearchengine.desktop,
	  branches/KDE/3.5/kdesdk/kdeaccounts-plugin/kdeaccountsplugin.desktop,
	  branches/KDE/3.5/kdesdk/kompare/komparenavtreepart/komparenavtreepart.desktop,
	  branches/KDE/3.5/kdesdk/kfile-plugins/c++/kfile_h.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/kbabeldict/modules/dbsearchengine2/dbsearchengine2.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/commonui/kbabel_tool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/pluralforms/kbabel_pluralformstool.desktop,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/x-umbrello.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/equations/kbabel_equationstool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/addons/kfile-plugins/kfile_po.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/filters/gettext/kbabel_gettext_import.desktop,
	  branches/KDE/3.5/kdesdk/kbugbuster/kbugbuster.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/commonui/kbabel_validator.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/addons/preview/pothumbnail.desktop,
	  branches/KDE/3.5/kdesdk/kompare/kompare.desktop,
	  branches/KDE/3.5/kdesdk/cervisia/cvsservice/cvsservice.desktop,
	  branches/KDE/3.5/kdesdk/kfile-plugins/ts/kfile_ts.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/filters/gettext/kbabel_gettext_export.desktop,
	  branches/KDE/3.5/kdesdk/kompare/komparepart/komparepart.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/length/kbabel_lengthtool.desktop,
	  branches/KDE/3.5/kdesdk/kbugbuster/kresources/bugzilla.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/kbabeldict/modules/tmx/tmxcompendium.desktop,
	  branches/KDE/3.5/kdesdk/kfile-plugins/c++/kfile_cpp.desktop,
	  branches/KDE/3.5/kdesdk/kompare/kompareviewpart.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/filters/linguist/kbabel_linguist_import.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/datatools/context/kbabel_contexttool.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/kbabel/kbabel.desktop,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umbrello.desktop,
	  branches/KDE/3.5/kdesdk/kuiviewer/kuiviewer.desktop,
	  branches/KDE/3.5/kdesdk/kbabel/filters/linguist/kbabel_linguist_export.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-07-06 22:01 +0000 [r828896]  ilic

	* branches/KDE/3.5/kdesdk/scripts/extractrc: Try to fix a problem
	  of some messages omitted by xgettext.

2008-08-19 19:48 +0000 [r849605]  coolo

	* branches/KDE/3.5/kdesdk/kdesdk.lsm: update for 3.5.10

