------------------------------------------------------------------------
r854528 | ilic | 2008-08-29 20:23:33 +0200 (Fri, 29 Aug 2008) | 1 line

Name catalog same as component name.
------------------------------------------------------------------------
r856760 | lueck | 2008-09-03 20:18:11 +0200 (Wed, 03 Sep 2008) | 4 lines

move the kcontrol docs into kcontrol subdir
move in trunk Revision 856754
Could someone please mv kdeadmin/knetworkconf.po kdeadmin/kcontrol_knetworkconf.po
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r857609 | lueck | 2008-09-05 22:16:30 +0200 (Fri, 05 Sep 2008) | 1 line

fix wrong url
------------------------------------------------------------------------
r859701 | scripty | 2008-09-11 08:09:21 +0200 (Thu, 11 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r863589 | lueck | 2008-09-22 17:41:46 +0200 (Mon, 22 Sep 2008) | 1 line

added missing build file
------------------------------------------------------------------------
