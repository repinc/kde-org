------------------------------------------------------------------------
r1122720 | winterz | 2010-05-05 02:51:27 +1200 (Wed, 05 May 2010) | 10 lines

Backport r1122711 by winterz from trunk to the 4.4 branch:

forward port SVN commit 1122653 by smartins:

timesInInterval wasn't working properly on recurrences with duration, when we pass the last date as start.

the beginning of the interval is inclusive, so we shouldn't discard.
MERGE: 4.4


------------------------------------------------------------------------
r1122722 | winterz | 2010-05-05 02:55:02 +1200 (Wed, 05 May 2010) | 3 lines

revertlast.
although this will be needed if the new libical ever gets released.

------------------------------------------------------------------------
r1125448 | tmcguire | 2010-05-12 00:13:17 +1200 (Wed, 12 May 2010) | 6 lines

Backport r1125447 by tmcguire from trunk to the 4.4 branch:

Don't include dots at the end of URLs in the URL highlighting.

They are most commonly the word/sentence seperators instead.

------------------------------------------------------------------------
r1126859 | scripty | 2010-05-15 14:01:46 +1200 (Sat, 15 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1128471 | bcooksley | 2010-05-19 23:44:23 +1200 (Wed, 19 May 2010) | 1 line

Ensure compatibility with KDE 4.5 System Settings categories whilst keeping 4.4 compatibility
------------------------------------------------------------------------
r1129209 | guymaurel | 2010-05-22 04:04:04 +1200 (Sat, 22 May 2010) | 1 line

invert the order of the two first lines of kdemain
------------------------------------------------------------------------
r1131007 | winterz | 2010-05-27 12:09:07 +1200 (Thu, 27 May 2010) | 6 lines

backport SVN commit 1131005 by winterz from trunk:

fix a crash that can happen when formatting an invitation with a cancel
request but the invitation is empty.
BUG: 238945

------------------------------------------------------------------------
r1131525 | mueller | 2010-05-28 22:16:09 +1200 (Fri, 28 May 2010) | 2 lines

KDE 4.4.4

------------------------------------------------------------------------
