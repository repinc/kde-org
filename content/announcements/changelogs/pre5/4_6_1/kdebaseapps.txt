commit 022be127881819e0624575ccd8da3671ee5ce87e
Author: Dirk Mueller <mueller@kde.org>
Date:   Fri Feb 25 23:34:05 2011 +0100

    bump requires for KDE 4.6.1

commit 29fd719475b151e17fe79328bce6f65c0671d496
Author: Script Kiddy <scripty@kde.org>
Date:   Thu Feb 24 15:31:18 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit afb18d660a768d3bf78298eb094eb5c66b604c4f
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Feb 22 15:37:19 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit ad80c373561e5f13596989352e838a3b8c90bd4e
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Feb 21 14:29:58 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit c83be654889511cee7772677f7a1dd536792cea5
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Feb 20 14:12:17 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit db830db2812017707fbe7fd84ecc548a05c6e8f4
Author: Script Kiddy <scripty@kde.org>
Date:   Sat Feb 19 14:32:27 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit b668bf257b301aecef226c6a55512c61d0b7abc3
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Feb 18 14:47:06 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 86cdfd72ec24c3de7251bdbfcf5b47cec6686a2b
Author: Script Kiddy <scripty@kde.org>
Date:   Thu Feb 17 16:25:27 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 04be6256c1996ec4623ad7d697d3c854948b9f19
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Feb 16 14:10:54 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 4c6b38737609925d424e322ae20c5e4c5c8843d0
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Feb 15 15:41:37 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit f6009e974084e25f9e8534a36a02085a19a15037
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Feb 14 22:24:57 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 75c8cbc51889b4c344bff37d3c72a6b9c5b828ef
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Feb 14 04:01:55 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 26e3499943477370dfdb9d0ccfd0ddca53c99c86
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Sun Feb 13 13:19:12 2011 +0100

    Improve performance when turning on the preview mode
    
    If a change from a directory with disabled previews is done to a directory with enabled previews, also previews are generated for the previous directory as the preview-generator still contains the not updated directory lister. Because of this it is important to apply the view-properties after the directory-lister has been updated.

commit ea82e0f9df247b79cd809641818324693ed40fd0
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Feb 13 02:20:25 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 5791fc023f702c145696deb9ecc76a78104e7389
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Sat Feb 12 18:22:05 2011 +0100

    Open folder in a new tab when a middle-click is done in the column-view
    
    BUG: 265859
    FIXED-IN: 4.6.1

commit d5504ca8891fb01ed2f5665f33d8e928a06ebaa1
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Sat Feb 12 17:46:24 2011 +0100

    Reset progress of the statusbar when the stop-button has been pressed

commit 720f6c743eea156620943e009551ec12be130deb
Author: Alberto Villa <avilla@FreeBSD.org>
Date:   Fri Feb 11 22:51:28 2011 +0100

    Avoid chfn execution on FreeBSD
    
    FreeBSD chfn/chpass doesn't support option -f, so don't try to change
    user's full name in /etc/passwd.

commit 82a1ffa067844e6ce14e86c310086e1958f67839
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Feb 11 13:26:16 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 713462a0cda593dd2c85bb0c221aba332f84edff
Author: David Faure <faure@kde.org>
Date:   Fri Feb 11 00:57:37 2011 +0100

    Fix many problems in the completion box of the searchbar
    
    * clearSuggestions was crashing to due unexpected reentrancy
    (takeItem -> currentTextChanged -> textEdited -> clearSuggestions)
    BUG: 265484
    
    * completion box would pop up -after- hitting return, so it would eat
    the first click in the webpage
    BUG: 231213
    
    * pressing the mouse on a suggestion item would also lead to a text change which
    would clear suggestions, hard to use; solved by not doing this while the mouse
    button is down. This problem still kind of happens with key down/up, but isn't
    solvable under the current KLineEdit logic of "choosing text in completion popup
    is equivalent to typing it".

commit fc7f59a49fc7b7a9b6256638e042b19cb9e96cbf
Author: Christophe Giboudeaux <cgiboudeaux@gmail.com>
Date:   Mon Jan 3 20:40:10 2011 +0000

    Cleanup, include the correct directory.
    
    svn path=/trunk/KDE/kdebase/apps/; revision=1211360
    (cherry picked from commit 97d48161f5d8b647f3fc76fe4d8ae6b5932024ff)

commit d2f83a3b52620044e8327c6d616c06dda384a0ff
Author: David Faure <faure@kde.org>
Date:   Mon Jan 3 09:44:41 2011 +0000

    Move konq-plugins into kde-baseapps in the 4.6 branch too.
    
    Otherwise there is nowhere to get them from, for 4.6: extragear is
    gone from svn, and has no 4.6 branch of it.
    
    svn path=/trunk/KDE/kdebase/apps/; revision=1211211
    (cherry picked from commit c9ab6afc71a57f59e2eb3e4bb77da7e3704f6a96)

commit ec05ba261fffa6d9a4d301e5ee64d3922a6d020d
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Wed Feb 9 22:04:59 2011 +0100

    Assure that RETURN emits itemTriggered() for one file
    
    If only one file is selected, pressing RETURN should behave similar like triggering the item with the mouse. For this the signal itemTriggered() must be emitted.
    
    BUG: 250475

commit 6bb8f5ba17be70237972f805e54b3d6491bb6574
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Wed Feb 9 21:37:01 2011 +0100

    Fix regression that creating a sub-folder is not possible
    
    BUG: 265714

commit 4caa285c065ea362ae52d93a567680513d8beb2a
Author: David Faure <faure@kde.org>
Date:   Mon Feb 7 13:07:38 2011 +0100

    Add support for KAbstractFileItemActionPlugin. Requires kdelibs > 4.6.0.

commit edb8046f76413f872f49b520fbab309e75a8954d
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Sun Feb 6 13:51:57 2011 +0100

    Fix issue that filenamesearch: is used instead of nepmuksearch:

commit e6467c09ac3096ca073c1195ed698e0a1d6ffeda
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Sat Feb 5 21:09:36 2011 +0100

    Fix compilation when Nepomuk is not available
    
    BUG: 265547

commit 17ea44b9c3c0f4eb77d03fda6cec5d31bc095de3
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Fri Feb 4 21:18:25 2011 +0100

    Don't clear the search-text when switching between tabs
    
    The clearing of the search-text should only be done when opening the search-box.

commit 104e03d5b3b4734f333acb8560ffc9b3112d2113
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Feb 4 12:28:26 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 07905d637c4d849b8a45bf8b237f94f11671ee72
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Thu Feb 3 17:22:38 2011 +0100

    Use KUrl::isParentOf() instead of doing custom QString-operations
    
    This fixes the issue that a directory might be declared as indexed/non-indexed although it just starts with a similar name like another directory.

commit c2c3a87985b7ab5b13ba9b7833b85410e03f6c4c
Author: David Faure <faure@kde.org>
Date:   Wed Feb 2 23:47:08 2011 +0100

    Fix change compression when editing fields: showBookmark was committing changes
    
    and notifyManagers() was leading to a third save and a second full-tree-reload.
    Removed these calls, since command::redo does it already.
    
    Also fixed change compression messing up the "old value", so undo didn't restore
    the orig value.
    
    FIXED-IN: 4.6.1
    BUG: 242884
    (cherry picked from commit 1a509e8dea82f026a317508c4fa084c29f337bf9)

commit 9379223f964c3d6f69769e4685bb3ac6fdb1dd1a
Author: David Faure <faure@kde.org>
Date:   Wed Feb 2 23:44:57 2011 +0100

    Only connect to the changed() signal, otherwise the slot is called twice
    
    and the second time reloads the whole tree
    (bookmarksChanged is the DBUS signal, so changed gets emitted afterwards)
    CCBUG: 242884
    (cherry picked from commit 4a9c92b29fb17db2587efb1c485068f860b3896d)

commit dc9e9d4f1b321da4d7d5f462910144684adcc408
Author: David Faure <faure@kde.org>
Date:   Wed Feb 2 23:06:37 2011 +0100

    Fix "undo" undoing two steps, and "redo" redoing two steps
    (cherry picked from commit 2dee96208d698082dcc0f7bab74ae1c0694850e9)

commit ac4c4f3ebc6f0394aac4c7213468258603df9905
Author: Ian Monroe <imonroe@kde.org>
Date:   Wed Feb 2 16:32:05 2011 -0600

    Fix cmake for konsole doc removal

commit 774b0c9d259a516f2f5335a785d21b141d83e7eb
Author: Ian Monroe <imonroe@kde.org>
Date:   Wed Feb 2 15:59:58 2011 -0600

    Delete konsole docs

commit d54313ecd8a4e9ec1bbe11b8264ea20114f02714
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Wed Feb 2 19:33:56 2011 +0100

    Fix visibility- and enabled-issues for the filter-panel
    
    The filter-panel should be disabled if the current folder is not indexed at all. Also when triggering a "Find" the filter-panel should stay invisible per default when the current folder is not indexed.
    
    BUG: 264969
    FIXED-IN: 4.6.1

commit e687304d6a8e411d6245db08a2ca25e27eb54395
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Wed Feb 2 17:37:35 2011 +0100

    Provide a default size-hint for all panels
    
    This fixes the issue that the initial size of the Filter Panel is too small.

commit 1ab02e36e9599fa3a6325a1840cdd22e1d30f433
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Feb 2 14:00:40 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 69526109af7fd4d058442fcf4d00d7e6e73a5c2a
Author: Valentin Rusu <kde@rusu.info>
Date:   Tue Feb 1 00:22:15 2011 +0100

    Fixed bug 244951 - crash when switching to midnight commander mode

commit 8c1cb5b79a6a90726112fbd00d3aab37d2090504
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Mon Jan 31 22:31:02 2011 +0100

    Show the pointing-hand cursor also when the selection-toggle is disabled
    
    BUG: 264791
    FIXED-IN: 4.6.1

commit 09d30e3590b27e6955512d4726e1b188430765f8
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Mon Jan 31 21:31:05 2011 +0100

    Don't show the pointing-hand cursor when double-click is enabled
    
    BUG: 264796
    FIXED-IN: 4.6.1

commit ed2de2e5b6a5cb717d3cbbf82d65f46cbc3df697
Merge: 40e6585 39ef233
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sun Jan 30 12:58:59 2011 -0800

    Merge remote branch 'origin/4.6' into 4.6

commit 40e65858d62915c6ea57c48207242729ea063053
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sun Jan 30 12:13:37 2011 -0800

    fix 2-click drag, w/correct painting on selection
    (cherry picked from commit 0714dda8df441519009961148318011b6577c71b)

commit 39ef23341940fd623eb6a28defb8bfe6dc5021e2
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Sun Jan 30 21:04:24 2011 +0100

    Updated version and copyright-information

commit 648d6ec136072a5e02e1c855ebe5ce7b963b6a24
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Jan 28 13:03:21 2011 +0000

    SVN_SILENT made messages (.desktop file)
    
    svn path=/branches/KDE/4.6/kdebase/apps/; revision=1217757

commit c61155d6959a0d597c9305584e11dacf94227553
Author: Sebastian Doerner <sebastian@sebastian-doerner.de>
Date:   Thu Jan 27 22:47:39 2011 +0000

    Backport of commit 1217560
    
    Fix crash when detaching a tab with "Split View Mode" setting enabled
    
    svn path=/branches/KDE/4.6/kdebase/apps/; revision=1217573

commit 0e4914cbf4cc9fd593f1d7c54835fe6c4c1341f6
Author: Aaron J. Seigo <aseigo@kde.org>
Date:   Thu Jan 27 21:29:47 2011 +0000

    make hover triggering configurable; will backport for 4.6.1 as well
    CCMAIL:wstephenson@kde.org
    
    svn path=/branches/KDE/4.6/kdebase/apps/; revision=1217559

commit 3ad99c167256a0b455eb045920aa8e8d4fec95dc
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Jan 25 13:53:56 2011 +0000

    SVN_SILENT made messages (.desktop file)
    
    svn path=/branches/KDE/4.6/kdebase/apps/; revision=1217036

commit e146259a25094d667ae80cc4405fdf04655d3c9e
Author: Kurt Hindenburg <kurt.hindenburg@gmail.com>
Date:   Sun Jan 23 17:41:30 2011 +0000

    Remove unused code that was used in the previous menu stucture.
    
    This removes the warning:
    konsole(11694)/kdeui (kdelibs): Attempt to use QAction "" with KXMLGUIFactory!
    
    CCBUG: 183244
    
    svn path=/branches/KDE/4.6/kdebase/apps/; revision=1216491

commit 02048a6021969dddd1e33c6bcbaa94e30cf9044e
Author: Kurt Hindenburg <kurt.hindenburg@gmail.com>
Date:   Sun Jan 23 17:10:06 2011 +0000

    update version
    
    svn path=/branches/KDE/4.6/kdebase/apps/; revision=1216487

commit b9232d04c64605ba99b8eb09adeb3dbba828ee67
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Sun Jan 23 15:03:42 2011 +0000

    Prevent that the view is forced to increase its width when the searching is enabled and not enough width is available for all options.
    
    svn path=/branches/KDE/4.6/kdebase/apps/; revision=1216466

commit c953908b0d4d82956a726e3aa3b66e5f85ffc163
Author: Albert Astals Cid <tsdgeos@terra.es>
Date:   Sat Jan 22 18:19:37 2011 +0000

    set the SOPRANO_MIN_VERSION so nepomuk is correctly not found in my machine
    
    svn path=/branches/KDE/4.6/kdebase/apps/; revision=1216326

commit efaed0ed33c2fefe26b877389318a9945ebe42ee
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Jan 21 12:02:04 2011 +0000

    SVN_SILENT made messages (.desktop file)
    
    svn path=/branches/KDE/4.6/kdebase/apps/; revision=1216106
