---
aliases:
- ../../kde-frameworks-5.23.0
date: 2016-06-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---

### Attica

- Make it actually possible to tell providers from the url we were given
- Provide QDebug helpers for some Attica classes
- Fix redirection of absolute Urls (bug 354748)

### Baloo

- Fix using spaces in the tags kioslave (bug 349118)

### Breeze Icons

- Add a CMake option to build binary Qt resource out of icons dir
- Many new and updated icons
- update disconnect network icon for bigger diference to conntected (bug 353369)
- update mount and unmount icon (bug 358925)
- add some avatars from plasma-desktop/kcms/useraccount/pics/sources
- remove chromium icon cause the default chromium icon fit's well (bug 363595)
- make the konsole icons lighter (bug 355697)
- add mail icons for thunderbird (bug 357334)
- add public key icon (bug 361366)
- remove process-working-kde cause the konqueror icons should be used (bug 360304)
- update krusader icons according to (bug 359863)
- rename the mic icons according D1291 (bug D1291)
- add some script mimetype icons (bug 363040)
- add virtual keyboard and touchpad on/off functionality for OSD

### Framework Integration

- Remove unused dependencies and translation handling

### KActivities

- Adding runningActivities property to the Consumer

### KDE Doxygen Tools

- Major rework of the API docs generation

### KCMUtils

- Use QQuickWidget for QML KCMs (bug 359124)

### KConfig

- Avoid skipping KAuthorized check

### KConfigWidgets

- Allow using new style connect syntax with KStandardAction::create()

### KCoreAddons

- Print the failing plugin when notifying a cast warning
- [kshareddatacache] Fix invalid use of &amp; to avoid unaligned reads
- Kdelibs4ConfigMigrator: skip reparsing if nothing was migrated
- krandom: Add testcase to catch bug 362161 (failure to auto-seed)

### KCrash

- Check size of unix domain socket path before copying to it

### KDeclarative

- Support selected state
- KCMShell import can now be used to query for whether opening a KCM is actually allowed

### KDELibs 4 Support

- Warn about KDateTimeParser::parseDateUnicode not being implemented
- K4TimeZoneWidget: correct path for flag images

### KDocTools

- Add commonly used entities for keys to en/user.entities
- Update man-docbook template
- Update book template + man template + add arcticle template
- Call kdoctools_create_handbook only for index.docbook (bug 357428)

### KEmoticons

- Add emojis support to KEmoticon + Emoji One icons
- Add support for custom emoticon sizes

### KHTML

- Fix potential memory leak reported by Coverity and simplify the code
- The number of layers is determined by the number of comma-separated values in the ‘background-image’ property
- Fix parsing background-position in shorthand declaration
- Do not create new fontFace if there is no valid source

### KIconThemes

- Don't make KIconThemes depend on Oxygen (bug 360664)
- Selected state concept for icons
- Use system colors for monochrome icons

### KInit

- Fix race in which the file containing the X11 cookie has the wrong permissions for a small while
- Fix permissions of /tmp/xauth-xxx-_y

### KIO

- Give clearer error message when KRun(URL) is given a URL without scheme (bug 363337)
- Add KProtocolInfo::archiveMimetypes()
- use selected icon mode in file open dialog sidebar
- kshorturifilter: fix regression with mailto: not prepended when no mailer is installed

### KJobWidgets

- Set correct "dialog" flag for Progress Widget dialog

### KNewStuff

- Don't initialize KNS3::DownloadManager with the wrong categories
- Extend KNS3::Entry public API

### KNotification

- use QUrl::fromUserInput to construct sound url (bug 337276)

### KNotifyConfig

- use QUrl::fromUserInput to construct sound url (bug 337276)

### KService

- Fix associated applications for mimetypes with uppercase characters
- Lowercase the lookup key for mimetypes, to make it case insensitive
- Fix ksycoca notifications when the DB doesn't exist yet

### KTextEditor

- Fix default encoding to UTF-8 (bug 362604)
- Fix color configurability of default style "Error"
- Search &amp; Replace: Fix replace background color (regression introduced in v5.22) (bug 363441)
- New color scheme "Breeze Dark", see https://kate-editor.org/?post=3745
- KateUndoManager::setUndoRedoCursorOfLastGroup(): pass Cursor as const reference
- sql-postgresql.xml improve syntax highlighting by ignoring multiline function bodies
- Add syntax highlighting for Elixir and Kotlin
- VHDL syntax highlighting in ktexteditor: add support for functions inside architecture statements
- vimode: Don't crash when given a range for a nonexistent command (bug 360418)
- Properly remove composed characters when using Indic locales

### KUnitConversion

- Fix downloading currency exchange rates (bug 345750)

### KWallet Framework

- KWalletd migration: fix error handling, stops the migration from happening on every single boot.

### KWayland

- [client] Don't check resource version for PlasmaWindow
- Introduce an initial state event into Plasma Window protocol
- [server] Trigger error if a transient request tries to parent to itself
- [server] Properly handle the case that a PlasmaWindow is unmapped before client bound it
- [server] Properly handle destructor in SlideInterface
- Add support for touch events in fakeinput protocol and interface
- [server] Standardize the destructor request handling for Resources
- Implement wl_text_input and zwp_text_input_v2 interfaces
- [server] Prevent double delete of callback resources in SurfaceInterface
- [server] Add resource nullptr check to ShellSurfaceInterface
- [server] Compare ClientConnection instead of wl_client in SeatInterface
- [server] Improve the handling when clients disconnect
- server/plasmawindowmanagement_interface.cpp - fix -Wreorder warning
- [client] Add context pointer to connects in PlasmaWindowModel
- Many fixes related to destruction

### KWidgetsAddons

- Use selected icon effect for current KPageView page

### KWindowSystem

- [platform xcb] Respect request icon size (bug 362324)

### KXMLGUI

- Right-clicking the menu bar of an application will now longer allow bypassing

### NetworkManagerQt

- Revert "drop WiMAX support for NM 1.2.0+" as it breaks ABI

### Oxygen Icons

- Sync weather icons with breeze
- Add update icons

### Plasma Framework

- Add cantata system tray support (bug 363784)
- Selected state for Plasma::Svg and IconItem
- DaysModel: reset m_agendaNeedsUpdate when plugin sends new events
- Update audio and network icon to get a better contrast (bug 356082)
- Deprecate downloadPath(const QString &amp;file) in favor of downloadPath()
- [icon thumbnail] Request for preferred icon size (bug 362324)
- Plasmoids can now tell whether widgets are locked by the user or sysadmin restrictions
- [ContainmentInterface] Don't try to popup empty QMenu
- Use SAX for Plasma::Svg stylesheet replacement
- [DialogShadows] Cache access to QX11Info::display()
- restore air plasma theme icons from KDE4
- Reload selected color scheme on colors changed

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
