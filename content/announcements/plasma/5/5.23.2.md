---
date: 2021-10-26
changelog: 5.23.1-5.23.2
layout: plasma
asBugfix: true
draft: false
---

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.23/Plasma_25AE_Final.mp4" poster="https://kde.org/announcements/plasma/5/5.23.0/Plasma25AE_b.png" >}}

+ Plasma Browser Integration: [History Runner] Skip blob URLs. [Commit.](http://commits.kde.org/plasma-browser-integration/9ce049c9b93304afe83ac45a84f25a623fe71326)
+ [Folder View] Fix executing file without prompting. [Commit.](http://commits.kde.org/plasma-desktop/085f9b7e520a5b5f921a5b9a353d20be73c3d4ad) Fixes bug [#435560](https://bugs.kde.org/435560)
+ Desktop as folder: restore functionality of the "delete" action. [Commit.](http://commits.kde.org/plasma-desktop/9f53bf160b347f76f2d4914408ac9bad6975275b) Fixes bug [#442765](https://bugs.kde.org/442765)
