<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdeaccessibility-4.6.2.tar.bz2">kdeaccessibility-4.6.2</a></td><td align="right">5,0MB</td><td><tt>5e8e92a312b2c995602a66cc775009839122053d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdeadmin-4.6.2.tar.bz2">kdeadmin-4.6.2</a></td><td align="right">776KB</td><td><tt>33d756b1cef42d272b617bfba400e9116b34f311</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdeartwork-4.6.2.tar.bz2">kdeartwork-4.6.2</a></td><td align="right">112MB</td><td><tt>2a264081e50f2d2d78d98080db8f013f3148f9b2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdebase-4.6.2.tar.bz2">kdebase-4.6.2</a></td><td align="right">2,6MB</td><td><tt>e9e46a6b4f5580996a1484da6d2ef17f418eec69</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdebase-runtime-4.6.2.tar.bz2">kdebase-runtime-4.6.2</a></td><td align="right">5,6MB</td><td><tt>d939d78f82e8c9d1257552abac08e4da1ff28c51</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdebase-workspace-4.6.2.tar.bz2">kdebase-workspace-4.6.2</a></td><td align="right">67MB</td><td><tt>63efb68733f347d0b7d24a7a061c5a0e3f84dee7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdebindings-4.6.2.tar.bz2">kdebindings-4.6.2</a></td><td align="right">6,8MB</td><td><tt>eff4b5785b09958fffe4e23583f5af75c796aa9e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdeedu-4.6.2.tar.bz2">kdeedu-4.6.2</a></td><td align="right">69MB</td><td><tt>7c94a10f5c17237dcd5c348a8ac129973934cf05</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdegames-4.6.2.tar.bz2">kdegames-4.6.2</a></td><td align="right">57MB</td><td><tt>91ec7347488504931b4019182a3f2b91a60d8065</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdegraphics-4.6.2.tar.bz2">kdegraphics-4.6.2</a></td><td align="right">5,0MB</td><td><tt>9c9f31c88c1c9e6dcaa7fb177847402d874ffc94</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdelibs-4.6.2.tar.bz2">kdelibs-4.6.2</a></td><td align="right">13MB</td><td><tt>b4794a9a69492ce4725abd9eb5d00d3a50e46603</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdemultimedia-4.6.2.tar.bz2">kdemultimedia-4.6.2</a></td><td align="right">1,6MB</td><td><tt>4447d26488e3b97c43580d86ad47cee77ff98578</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdenetwork-4.6.2.tar.bz2">kdenetwork-4.6.2</a></td><td align="right">8,3MB</td><td><tt>6865c074b87a201b876d11b86a2f14aff5f45abb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdepimlibs-4.6.2.tar.bz2">kdepimlibs-4.6.2</a></td><td align="right">3,1MB</td><td><tt>f3c78688203b90efabdd8961783e8f4d16d9c57c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdeplasma-addons-4.6.2.tar.bz2">kdeplasma-addons-4.6.2</a></td><td align="right">1,9MB</td><td><tt>f888908e937453c9799c6ad71c514a5227e13700</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdesdk-4.6.2.tar.bz2">kdesdk-4.6.2</a></td><td align="right">5,9MB</td><td><tt>d246f5571aa772d4a4d0538456b142799c17c146</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdetoys-4.6.2.tar.bz2">kdetoys-4.6.2</a></td><td align="right">396KB</td><td><tt>4619d2124abe0887ce5aca24f1333419f7cd88c5</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdeutils-4.6.2.tar.bz2">kdeutils-4.6.2</a></td><td align="right">3,6MB</td><td><tt>621d77f8f75bdd867316768f48ea257829f7def1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/kdewebdev-4.6.2.tar.bz2">kdewebdev-4.6.2</a></td><td align="right">2,2MB</td><td><tt>80f709845fb871919fbb2c293d1cc8f40160509b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.2/src/oxygen-icons-4.6.2.tar.bz2">oxygen-icons-4.6.2</a></td><td align="right">302MB</td><td><tt>dc97ca3e52bd00f93fb8438e8f368aa3c1567d9f</tt></td></tr>
</table>
