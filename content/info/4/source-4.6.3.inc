<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdeaccessibility-4.6.3.tar.bz2">kdeaccessibility-4.6.3</a></td><td align="right">5,0MB</td><td><tt>8d4053defbf839fd428820d6d1659b6e46ab468a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdeadmin-4.6.3.tar.bz2">kdeadmin-4.6.3</a></td><td align="right">780KB</td><td><tt>141e8e794ebd5341940d2d22986b7376a2abefbf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdeartwork-4.6.3.tar.bz2">kdeartwork-4.6.3</a></td><td align="right">112MB</td><td><tt>084834ac3c654ea24257d9ed407c2384bf5aa110</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdebase-4.6.3.tar.bz2">kdebase-4.6.3</a></td><td align="right">2,6MB</td><td><tt>69cfba74099cdce27f7f72725288f025979b41f3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdebase-runtime-4.6.3.tar.bz2">kdebase-runtime-4.6.3</a></td><td align="right">5,6MB</td><td><tt>75cf1993b42cbada44b8c3ca2d5b99e6ac48594c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdebase-workspace-4.6.3.tar.bz2">kdebase-workspace-4.6.3</a></td><td align="right">67MB</td><td><tt>50103b7752defbab08965b665fa95361bb5b3ff6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdebindings-4.6.3.tar.bz2">kdebindings-4.6.3</a></td><td align="right">6,8MB</td><td><tt>4ecc6ac378c241472aa75680abfafd8d57672653</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdeedu-4.6.3.tar.bz2">kdeedu-4.6.3</a></td><td align="right">69MB</td><td><tt>4b27e6e1670760982dae13dc8c51c814c6853396</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdegames-4.6.3.tar.bz2">kdegames-4.6.3</a></td><td align="right">57MB</td><td><tt>472cb9541f389a8869a496f715d48587db568dda</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdegraphics-4.6.3.tar.bz2">kdegraphics-4.6.3</a></td><td align="right">5,0MB</td><td><tt>c16dc9c55e7459418fa9887aa532ea0a269e7179</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdelibs-4.6.3.tar.bz2">kdelibs-4.6.3</a></td><td align="right">13MB</td><td><tt>c7fb089c9d52a6b1d9188b9e788753373a3288e4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdemultimedia-4.6.3.tar.bz2">kdemultimedia-4.6.3</a></td><td align="right">1,6MB</td><td><tt>d5bde19156b65e9b56e0e6de800efc0efe117632</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdenetwork-4.6.3.tar.bz2">kdenetwork-4.6.3</a></td><td align="right">8,3MB</td><td><tt>6f140dca442be815eda4fb79d51efd9e4588a1f1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdepimlibs-4.6.3.tar.bz2">kdepimlibs-4.6.3</a></td><td align="right">3,1MB</td><td><tt>370b4e98ab9e1f8ba18d36630d5c8bbff8da4883</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdeplasma-addons-4.6.3.tar.bz2">kdeplasma-addons-4.6.3</a></td><td align="right">1,9MB</td><td><tt>7a43dcfebc0316e681b04d0958d89272620a11b7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdesdk-4.6.3.tar.bz2">kdesdk-4.6.3</a></td><td align="right">5,8MB</td><td><tt>6faecbd828fda6cf0aced642287d982d3541d746</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdetoys-4.6.3.tar.bz2">kdetoys-4.6.3</a></td><td align="right">396KB</td><td><tt>07eaf12ccabc9209bd5de6a3f0e4ff6c70fca8b0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdeutils-4.6.3.tar.bz2">kdeutils-4.6.3</a></td><td align="right">3,6MB</td><td><tt>1aa735c03689ef49b949278ee31d0d8e8536b7d6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/kdewebdev-4.6.3.tar.bz2">kdewebdev-4.6.3</a></td><td align="right">2,2MB</td><td><tt>39e72fbaf76ee1c685321c8c3a767c38e63bc2a9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.3/src/oxygen-icons-4.6.3.tar.bz2">oxygen-icons-4.6.3</a></td><td align="right">334MB</td><td><tt>9efaf92d516f716f3d36ff0e21c66384500e967d</tt></td></tr>
</table>
