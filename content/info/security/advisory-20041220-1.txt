-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: Konqueror Java Vulnerability
Original Release Date: 2004-12-20
URL: http://www.kde.org/info/security/advisory-20041220-1.txt

0. References

        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-1145
        http://www.heise.de/security/dienste/browsercheck/tests/java.shtml

1. Systems affected:

        All versions of KDE up to KDE 3.3.1 inclusive. KDE 3.3.2 is not
        affected.


2. Overview:

        Two flaws in the Konqueror webbrowser make it possible to by pass
        the sandbox environment which is used to run Java-applets.
        One flaw allows access to restricted Java classes via JavaScript,
        making it possible to escalate the privileges of the Java-applet.
        The other problem is that Konqueror fails to correctly restrict
        access to certain Java classes from the Java-applet itself.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2004-1145 to this issue.


3. Impact:

        When a user has Java enabled in Konqueror and visits a malicious
        website, the website can run a Java-applet and obtain escalated
        privileges allowing reading and writing of arbitrary files with
        the privileges of the user.


4. Solution:

        Upgrade to KDE 3.3.2

        A backport has been made available for older versions which fixes
        this vulnerability. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

	For KDE 3.2.3 a backport of the new Java handling is available from
	ftp://ftp.kde.org/pub/kde/security_patches : 

  7fc001d010c640738ed7d2fe347f002d  post-3.2.3-kdelibs-khtml-java.tar.bz2


6. Time line and credits:

        24/11/2004 security@kde.org contacted by heise Security
	29/11/2004 Fixed in KDE CVS by Koos Vriezen
        14/12/2004 Backport for KDE 3.2.3
	20/12/2004 KDE Advisory released
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.2 (GNU/Linux)

iD8DBQFBxrjhN4pvrENfboIRAgsQAKCS+UqFNRuAxDDP8BTud+vIFNoooQCgj8Fb
vL44UC+LsJnu++GdLpKAlm4=
=JFzK
-----END PGP SIGNATURE-----
