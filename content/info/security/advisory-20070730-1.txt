
KDE Security Advisory: kpdf/kword/xpdf stack based buffer overflow
Original Release Date: 2007-07-30
URL: http://www.kde.org/info/security/advisory-20070730-1.txt

0. References
         CVE-2007-3387

1. Systems affected:

        KDE 3.2.0 up to including KDE 3.5.7.


2. Overview:

        kpdf, the KDE pdf viewer, shares code with xpdf. xpdf contains
        a vulnerability that can cause a stack based buffer overflow
        via a PDF file that exploits an integer overflow in
        StreamPredictor::StreamPredictor(). We'd like to thank
        Derek Noonburg for bringing this issue to our attention.


3. Impact:

        Remotely supplied pdf files can be used to disrupt the kpdf
        viewer on the client machine and possibly execute arbitrary code.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KOffice 1.2.1 and newer is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        eb5e65cd5fadab128c1bc5ce2211126b  koffice-xpdf-CVE-2007-3387.diff

        Patch for KDE 3.3.2 and newer is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        d1b3248c6a7843ad3265d25adcf7aa2f  post-3.5.7-kdegraphics-CVE-2007-3387.diff


