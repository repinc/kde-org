KDE Security Advisory: KDM Local Privilege Escalation Vulnerability
Original Release Date: 2010-04-13
URL: http://www.kde.org/info/security/advisory-20100413-1.txt

0. References
	CVE-2010-0436


1. Systems affected:

	KDM as shipped with KDE SC 2.2.0 up to including KDE SC 4.4.2


2. Overview:

	KDM contains a race condition that allows local attackers to
	make arbitrary files on the system world-writeable. This can
	happen while KDM tries to create its control socket during
	user login. This vulnerability has been discovered by
	Sebastian Krahmer from the SUSE Security Team.

3. Impact:

	A local attacker with a valid local account can under
	certain circumstances make use of this vulnerability to
	execute arbitrary code as root.

4. Solution:

	Source code patches have been made available which fix these
	vulnerabilities. Contact your OS vendor / binary package provider
	for information about how to obtain updated binary packages.

5. Patch:

	A patch for KDE 4.3.x-4.4.x is available from
	ftp://ftp.kde.org/pub/kde/security_patches :

	68c1dfe76e80812e5e049bb599b3374e  kdebase-workspace-4.3.5-CVE-2010-0436.diff


