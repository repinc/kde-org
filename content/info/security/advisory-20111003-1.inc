<li>KSSL in KDE SC versions up to and including 4.7.1 have an input
validation error that can be exploited to cause a user to be shown
invalid dialogs, including an invalid security certificate. Rekonq
is also affected.
Read the <a href="/info/security/advisory-20111003-1.txt">detailed advisory</a>
for more information.
</li>
