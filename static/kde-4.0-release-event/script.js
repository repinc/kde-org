    window.addEvent('domready', function(){
        var panes = $$('.pane');
        panes.each(function(item, idx){
            item['fx'] = new Fx.Style(item, 'opacity', {duration:500, wait:true}).set(0);
            item['toggler'] = $$('#menu li')[idx];
            item.toggler['fx'] = new Fx.Style(item.toggler, 'background-color', {duration:250, wait:false});
        });
        panes.setStyle('display', 'none');
        var cur = $(document.URL.split('#')[1]) || panes[0];
        $$('#menu li').each(function(item, idx) {
            hide = function(x) {
                x.toggler.fx.start('#000000');
                x.toggler.setStyle('color', '#aaaaaa');
                x.fx.start(0);
                x.setStyle('display', 'none');
            }
            show = function(x) {
                x.toggler.fx.start([40, 40, 40]); // <- tht doesn't work either
                x.toggler.setStyle('color', [255, 255, 255]);
                x.setStyle('display', 'block');
                x.fx.start(1);
            }
            item.addEvent('click', function(e) {
                // do nothing if the clicked pane is already shown
                if (cur == panes[idx]) return;
                // Hide the current pane and show the new one
                hide(cur);
                cur = panes[idx];
                show(cur);
    //             e.stop(); <-- that throws an error !
            });
        });

        show(cur);
});

